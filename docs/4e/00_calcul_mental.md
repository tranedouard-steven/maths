---
author: ET
title: 🥋 Ceintures CM 4e
hide:
  - footer
---

!!! example ""
    === ":fontawesome-regular-file-pdf:"
        |Le corrigé des fiches|Les fiches d'entraînement|
        |:-:|:-:|
        |[PDF](https://nuage03.apps.education.fr/index.php/s/Ei47oxzEWQbxZJt){:target="_blank"}|[PDF](https://nuage03.apps.education.fr/index.php/s/MHbWPfYAnMQ8XA8){:target="_blank"}|


    === ":white_large_square: Blanche" 
        ??? question "1) Critères de divisibilité"
            Un nombre entier est divisible  
            - par 2 : s’il est pair (il finit par 0, 2, 4, 6 ou 8).  
            - par 5 : s’il finit par 5 ou 0.  
            - par 10 : s’il finit par 0.  
            - par 3 (ou 9) : si la somme des chiffres du nombre est divisible par 3 (ou 9).  

            ^^Exemple 1^^ : 345 est-il divisible par 9 ?  
            3 + 4 + 5 = 12.  
            12 n’est pas dans la table de 9 donc 345 n’est pas divisible par 9 (mais par 3, oui car 12 est dans la table des 3).  

            ^^Exemple 2^^ : 46 est-il divisible par 3 ?  
            4 + 6 = 10.  
            10 n’est pas dans la table des 3 donc 46 n’est pas divisible par 3.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 14 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5ND2~o=0,1,2,3,4~q=0.-1.-2.-3.-4.~p=~t=14~n=6){:target="_blank"}  

                :octicons-video-16: [Appliquer les critères de divisibilité](https://www.youtube.com/watch?v=BJDE6uOrmYQ){:target="_blank"}

        ??? question "2) Double, triple, quadruple"
            Pour prendre le double d'un nombre entier, on le multiplie par 2.  

            ^^Exemple 1^^ : Le double de 7 est 14 car 7 × 2 = 14.  
            
            Pour prendre le triple d'un nombre entier, on le multiplie par 3.  

            ^^Exemple 2^^ : Le triple de 7 est 21 car 7 × 3 = 21.  

             Pour prendre le quadruple d'un nombre entier, on le multiplie par 4 (c'est-à-dire par 2 et encore par 2).  

            ^^Exemple 4^^ : Le quadruple de 7 est 28 car 7 × 4 = 28.  

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/6359234bd79c10001842372c){:target="_blank"}

        ??? question "3) Tables de multiplication"
            !!! quote ""
                :material-weight-lifter: Maths Mentales : [10 questions - 12 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=0,1,2,3,4,5,6,7,8~q=0.0,1-1.0,1-2.0,1-3.0,1-4.0,1-5.0,1-6.0,1-7.0,1-8.0,1~p=~t=12~n=10){:target="_blank"}  

                :material-weight-lifter: [36 questions pour un champion](https://www.mpoulain.fr/multiplication/){:target="_blank"}

        ??? question "4) Multiplication par 10 ; 100 ; 1 000"
            Dans la multiplication par 10, 100 ou 1 000, le nombre devient plus grand : chaque chiffre gagne 1, 2 ou 3 rangs dans l’écriture décimale. Le **chiffre** des unités est le plus facile à repérer.  

            ^^Exemple^^ : 4**2**,6 × 100 = 4 **2**60  
            Le chiffre des unités, le **2**, devient le chiffre des centaines (multiplication par 100).  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 14 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8NE4~o=0,1,2,3,4,5,6,7,8~q=0.-1.-2.-3.-4.-5.-6.-7.-8.~p=~t=14~n=6){:target="_blank"}  

                :octicons-video-16: [Multiplier par 10, 100 ou 1 000](https://www.youtube.com/watch?v=pPnCPmWGqyo){:target="_blank"}

        ??? question "5) Conversion unités de masse et de longueur"
            Pour convertir des unités de **longueur** : km - hm - dam - m - dm - cm - mm  
            Pour convertir des unités de **masse** : kg - hg - dag - g - dg - cg - mg  
            1 tonne = 1 000 kg  

            ^^Exemple 1^^ : 3,4 m = 340 cm  

            ^^Exemple 2^^ : 9,8 cm = 0,0098 dam   

            ^^Exemple 3^^ : 0,5 g = 5 dg = 50 cg  

            Il est important d’être à l’aise avec 2 choses : le tableau des unités et savoir trouver le chiffre des unités d’un nombre décimal.  
            La conversion se fait en 2 étapes :  
            - On place le nombre donné en mettant le chiffre des unités dans la colonne de l’unité fournie.  
            - On écrit le nombre décimal en prenant comme nouveau chiffre des unités celui de la nouvelle unité (on "déplace la virgule"). On devra parfois combler avec des zéros.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=7MA1~o=0~q=0.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Convertir les unités de longueur](https://www.youtube.com/watch?v=a6rFbX2eRx4){:target="_blank"}

        ??? question "6) Priorités de calcul"
            Dans un calcul sans parenthèses et formé uniquement d’additions et de soustractions, les calculs s’effectuent de gauche à droite.  

            ^^Exemple 1^^ : **64 – 9** + 1 = **55** + 1 = 56  
 
            Dans un calcul sans parenthèses, la multiplication et la division sont effectuées en priorité sur l’addition et la soustraction.  

            ^^Exemple 2^^ : 5 + **3 × 9** = 5 + **27** = 32  
 
            Dans un calcul avec parenthèses, les calculs entre parenthèses sont effectués en priorité.  
            ^^Exemple 3^^ : (**6 + 7**) × (**14 - 4**) = **13** × **10** = 130  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND20~o=0~q=0.0,1,2,3~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer des calculs avec des priorités](https://www.youtube.com/watch?v=a-IG_bjKeJc){:target="_blank"}
     
    === ":yellow_square: Jaune"
        ??? question "1) Carré et cube d'un nombre relatif"
            ^^Exemple 1^^ : 3² = 3 × 3 = 9  

            ^^Exemple 2^^ : 5² = 5 × 5 = 25  

            ^^Exemple 3^^ : 2³ = 2 × 2 × 2 = 8  

            ^^Exemple 4^^ : (-3)³ = (-3) × (-3) × (-3) = -27  

            ^^Exemple 5^^ : (-4)² = (-4) × (-4) = 16  

            ^^Exemple 6^^ : -4² = - 4 × 4 = -16          

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/60215348ffac500d9ec28a97){:target="_blank"}  

                :octicons-video-16: [Appliquer la règle des signes](https://www.youtube.com/watch?v=l_BleoCE-3Y){:target="_blank"}

        ??? question "2) Division par 10 ; 100 ; 1 000"
            Dans la division par 10, 100 ou 1000, le nombre devient plus petit : chaque chiffre perd 1, 2 ou 3 rangs dans l’écriture décimale.  

            ^^Exemple 2^^ : 7 41**3** ÷ 100 = 74,1**3**  
            Le chiffre des unités, le **3**, devient le chiffre des centièmes (division par 100).  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8NE5~o=0,1,2,3,4,5,6,7,8~q=0.-1.-2.-3.-4.-5.-6.-7.-8.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Diviser par 10 ; 100 ; 1000](https://www.youtube.com/watch?v=pLlBl2V1CC4){:target="_blank"}

        ??? question "3) Périmètre de figures usuelles"
            Le périmètre d’une figure est la longueur de son contour.  

            - Formule du périmètre du carré : 4 × côté = 4 × c  
            - Formule du périmètre du rectangle : 2 × Longueur + 2 × largeur = 2 × L + 2 × l  
            - Formule du périmètre du cercle : 2 × π × rayon = 2 × π × r ou π × diamètre = π × d  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6MA1~o=5,4,0,1~q=0.-1.-4.-5.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Calculer le périmètre d'une figure](https://www.youtube.com/watch?v=w7n638xdT6E){:target="_blank"}  

                :octicons-video-16: [Calculer la longueur d'un cercle](https://www.youtube.com/watch?v=iKyAfCzKnu4){:target="_blank"}

        ??? question "4) Addition de nombres relatifs"
            Distance à zéro = le nombre sans son signe = partie numérique  
            Si on a deux nombres de **même signe** : on garde le signe commun et on additionne les distances à zéro.  

            ^^Exemple 1^^ : (+3) + (+4) = (+7) ou 7  
            ^^Exemple 2^^ : (−6) + (−7) = (−13)  
 
            Si on a deux nombres de **signes contraires** : on prend le signe du nombre qui a la plus grande distance à zéro et on soustrait les distances à zéro.  

            ^^Exemple 3^^ : (−6) + (+2) = (−4) car 6 est plus grand que 2 donc le résultat est négatif, puis 6 − 2 = 4  
            ^^Exemple 4^^ : (+9) + (−3) = (+6) car 9 est plus grand que 3 donc le résultat est positif, puis 9 − 3 = 6  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 14 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC1~o=1,0~q=0.-1.~p=~t=14~n=6){:target="_blank"}  

                :octicons-video-16: [Somme de deux nombres relatifs](https://www.youtube.com/watch?v=zzhfzRic7xg){:target="_blank"}

        ??? question "5) Conversion de durées Niveau 1"
            1 h = 60 min  
            1 min = 60 s  
            1 h = 3 600 s  

            ^^Exemple 1^^ : 150 min = 60 min + 60 min + 30 min = 2 h 30  

            ^^Exemple 2^^ : 135 s = 60 s + 60 s + 15 s = 2 min 15 s  

            ^^Exemple 3^^ : 4 h = 4 × 60 min = 240 min  
            
            ^^Exemple 4^^ : 3 min 18 = 3 × 60 s + 18 s = 180 s + 18 s = 198 s  

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/5fdb0bb8ebe82c0d17f18f9f){:target="_blank"}

        ??? question "6) Moitié, tiers, quart"
            Prendre la moitié d'un nombre revient à le diviser par 2.  
            Prendre le tiers d'un nombre revient à le diviser par 3.  
            Prendre le quart d'un nombre revient à le diviser par 4 (on divise par 2 deux fois de suite).  

            ^^Exemple 1^^ : 450 ÷ 3 = 150  

            ^^Exemple 2^^ : 36 ÷ 4 = 9 car 36 ÷ 2 = 18 et puis 18 ÷ 2 = 9  

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/5fdb0d9e3c971e0d6598e81f){:target="_blank"}
 
    === ":orange_square: Orange"
        ??? question "1) Nombres premiers ou non"
            Un nombre premier est un nombre qui n’est divisible que par 1 et lui-même.  
            Les plus petits nombres premiers sont 2, 3, 5, 7, 11, 13, 17...  
            Pour montrer qu'un nombre n'est pas premier, on peut utiliser les critères de divisibilité.  

            ^^Exemple^^ : 123 n'est pas premier car il est divisible par 3 (1 + 2 + 3 = 6 et 6 est dans la table de 3).  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5ND5~o=1~q=0.-1.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Reconnaître un nombre premier](https://www.youtube.com/watch?v=g9PLLhnCv88){:target="_blank"} 

        ??? question "2) Encadrer une racine carrée"
            Pour encadrer une racine carrée par deux nombres entiers consécutifs, il est important de connaître les racines carrées remarquables.  
            $\sqrt{1}$ = 1  
            $\sqrt{4}$ = 2  
            $\sqrt{9}$ = 3  
            $\sqrt{16}$ = 4  
            $\sqrt{25}$ = 5  
            $\sqrt{36}$ = 6  
            $\sqrt{49}$ = 7  
            $\sqrt{64}$ = 8  
            $\sqrt{81}$ = 9  
            $\sqrt{100}$ = 10  
            $\sqrt{121}$ = 11  
            $\sqrt{144}$ = 12  

            ^^Exemple 1^^ : ... < $\sqrt{3}$ < ... ?  
            On cherche les deux racines carrées remarquables les plus proches de $\sqrt{3}$, plus grand et plus petit pour l'encadrer :  
            $\sqrt{1}$ < $\sqrt{3}$ < $\sqrt{4}$.  
            Donc 1 < $\sqrt{3}$ < 2.

            ^^Exemple 2^^ : ... < $\sqrt{20}$ < ... ?  
            On cherche les deux racines carrées remarquables les plus proches de $\sqrt{20}$, plus grand et plus petit pour l'encadrer :  
            $\sqrt{16}$ < $\sqrt{20}$ < $\sqrt{25}$.  
            Donc 4 < $\sqrt{20}$ < 5.            

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 60 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=4NA3~o=0~q=0.~p=~t=60~n=6){:target="_blank"}  

                :octicons-video-16: [Encadrer une racine carrée par 2 entiers consécutifs 1](https://www.youtube.com/watch?v=bjS5LW-hgWk){:target="_blank"}  

                :octicons-video-16: [Encadrer une racine carrée par 2 entiers consécutifs 2](https://www.youtube.com/watch?v=m3q6VvfS8N4){:target="_blank"}

        ??? question "3) Calcul du 3e angle dans un triangle"
            Dans un triangle, la somme de ses 3 mesures d'angle vaut toujours 180°.  

            ^^Exemple^^ : ABC est un triangle tel que $\widehat{A}$ = 52° et $\widehat{B}$ = 25°. Combien mesure $\widehat{C}$ ?  
            Comme 52 + 25 = 77, $\widehat{C}$ mesure 180 - 77 = 103°.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5GB1~o=0,1,2,3~q=0.-1.-2.-3.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Calculer un angle dans un triangle](https://www.youtube.com/watch?v=x0UA6kbiDcM){:target="_blank"}

        ??? question "4) Addition et soustraction de fractions Niveau 1"
            On est dans le cas où les dénominateurs sont les mêmes.  
            Pour trouver le résultat, on ajoute les numérateurs et on garde le dénominateur commun.  

            ^^Exemple 1^^ : $\dfrac{5}{3} + \dfrac{8}{3} = \dfrac{5+8}{3} = \dfrac{13}{3}$  

            ^^Exemple 2^^ : $\dfrac{85}{100} - \dfrac{6}{100} = \dfrac{85-6}{100} = \dfrac{79}{100}$  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC7~o=1~q=1.~p=~t=15~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer une addition ou une soustraction de fractions de même dénominateur](https://www.youtube.com/watch?v=2-JfYiX6Wk4){:target="_blank"}

        ??? question "5) Addition et soustraction de relatifs"
            Soustraire un nombre relatif, c’est additionner l’opposé de ce nombre relatif.  
            L’opposé de −6 est 6. L’opposé de 3 est −3.  

            ^^Exemple 1^^ : (−6) − (+𝟑) = (−6) + (−𝟑) = −9  
            On garde le premier nombre. Le – de la soustraction devient un + et on change le +3 en son opposé −3.  
 
            ^^Exemple 2^^ : (+5) − (−4) = (+5) + (+4) = +9  
            On garde le premier nombre. Le – de la soustraction devient un + et on change le −4 en son opposé +4.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC2~o=0,1~q=0.-1.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer des additions et soustractions de nombres relatifs](https://www.youtube.com/watch?v=9L4lz1NMPoY){:target="_blank"}

        ??? question "6) Aires et volumes"

            - Formule de l'aire d'un carré : côté × côté = côté au carré = c²  
            - Formule de l'aire d'un rectangle : Longueur × largeur = L × l  
            - Formule de l'aire d'un triangle : base × hauteur ÷ 2 = b × h ÷ 2  
            - Formule de l'aire d'un disque : π × rayon × rayon = π × r × r = π × r²  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6MC1~o=0,1,2,3,4~q=0.-1.-2.-3.-4.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Calculer l'aire d'un disque](https://www.youtube.com/watch?v=y-PV5LNmqsM){:target="_blank"}

            - Formule du volume d'un cube : côté × côté × côté = c³  
            - Formule du volume d'un pavé droit : Longueur × largeur × hauteur = L × l × h  
            - Formule du volume d'un cylindre : aire(disque) × hauteur = π × rayon × rayon × hauteur = π × r² × h  
            - Formule du volume d'un prisme droit : aire(base) × hauteur  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [pavé droit - 10 questions](https://view.genial.ly/5fdb11e4ebe82c0d17f19111){:target="_blank"}  

                :material-weight-lifter: Maths Mentales : [cylindre et prisme droit - 6 questions](https://view.genial.ly/6077e4d8dc45270d2f8609b2){:target="_blank"}  

                :octicons-video-16: [Calculer le volume d'un cylindre](https://www.youtube.com/watch?v=eJ8BSaTIpYU){:target="_blank"}

    === ":green_square: Verte"
        ??? question "1) Mesure du 3ème côté d’un triangle rectangle"
            Pour trouver la mesure du 3e côté d'un triangle rectangle, on applique le théorème de Pythagore : " (hypoténuse)² = (côté angle droit)² + (autre côté angle droit)².  

            ^^Exemple 1^^ : Soit 𝐴𝐵𝐶 un triangle rectangle en 𝐴.  
            AB = 4 cm, AC = 5 cm, BC = ?  
            On cherche la longueur de l'hypoténuse [BC].  
            4² + 5² = 16 + 25 = 41 donc BC = $\sqrt{41}$ cm  
 
            ^^Exemple 2^^ : Soit 𝐴𝐵𝐶 un triangle rectangle en 𝐴.  
            BC = 6 m, AC = 2 m, AB = ?  
            On cherche la longueur d'un coté de l’angle droit [AB].  
            6² − 2² = 36 − 4 = 32 (Attention soustraction ici) donc AB = $\sqrt{32}$ m  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [4 questions - 30 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=4GB7~o=0,1~q=0.-1.~p=~t=30~n=4){:target="_blank"}  

                :octicons-video-16: [Appliquer le théorème de Pythagore 1](https://www.youtube.com/watch?v=M9sceJ8gzNc){:target="_blank"}  

                :octicons-video-16: [Appliquer le théorème de Pythagore 2](https://www.youtube.com/watch?v=9CIh6GGVu_w){:target="_blank"}  

        ??? question "2) Décomposition en produit de facteurs premiers"
            Un nombre premier est un nombre qui n’est divisible que par 1 et lui-même.  
            Les plus petits nombres premiers sont 2, 3, 5, 7, 11, 13, 17...  
            Tous les nombres (non premiers) peuvent s’écrire comme une multiplication de nombres premiers.  

            ^^Exemple^^ :  
            22 = 2 × 11  
            6 = 2 × 3  
            9 = 3 × 3  
            16 = 2 × 2 × 2 × 2  
            15 = 3 × 5  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=5ND1~o=0~q=0.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Décomposer un nombre en produits de facteurs premiers](https://www.youtube.com/watch?v=BlGaIqNz_pk){:target="_blank"}   

        ??? question "3) Pourcentage simple"
            50 % d'un nombre, c'est prendre la moitié.  
            25 % d'un nombre, c'est prendre le quart.  
            75 % d'un nombre, c'est prendre les trois quarts.  
            10 % d'un nombre, c'est prendre le dixième.  
            100 % d'un nombre, c'est prendre la totalité.  

            ^^Exemple 1^^ : 10 % de 60 € ?  
            60 ÷ 10 = **6 €**  

            ^^Exemple 2^^ : 25 % de 60 € ?  
            60 ÷ 4 = **15 €**  

            ^^Exemple 3^^ : 75 % de 60 € ?  
            60 ÷ 4 = 15, puis 15 × 3 = **45 €**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC10~o=0,2,4,5~q=0.0,1-2.0,1-4.0,1-5.0,1~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Appliquer un pourcentage](https://www.youtube.com/watch?v=2UVaPRdSMl0){:target="_blank"}

        ??? question "4) Multiplication et division de relatifs"
            Si les deux facteurs sont de même signe, le produit est positif.  

            ^^Exemple 1^^ : (-4) × (-3) = 12  
 
            Si les deux facteurs sont de signes contraires, le produit est négatif.  

            ^^Exemple 2^^ : (-4) × 6 = -24  

            La règle des signes s'applique aussi pour la division.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 14 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=5NC5~o=0~q=0.~p=~t=14~n=6){:target="_blank"}  

                :octicons-video-16: [Appliquer la règle des signes](https://www.youtube.com/watch?v=q-vHvhiizqY){:target="_blank"}

        ??? question "5) Addition et soustraction de fractions"
            On doit déjà commencer par mettre les fractions au même dénominateur, puis appliquer ce qui a été vu à la ceinture bleue.  
            On va donc multiplier le numérateur ET le dénominateur d’une fraction par un même nombre pour avoir un dénominateur commun.  

            ^^Exemple 1^^ : $\dfrac{4}{6} + \dfrac{5}{3} = \dfrac{4}{6} + \dfrac{5×2}{3×2} = \dfrac{4}{6} + \dfrac{10}{6} = \dfrac{16}{6}$  

            ^^Exemple 2^^ : $\dfrac{6}{15} - \dfrac{1}{5} = \dfrac{6}{15} - \dfrac{1×3}{5×3} = \dfrac{6}{15} - \dfrac{3}{15} = \dfrac{3}{15}$  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC7~o=2~q=2.0,1~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer des additions et soustractions de fractions](https://www.youtube.com/watch?v=wfzLW6oF7VY){:target="_blank"}

        ??? question "6) Calcul en remplaçant x par la valeur indiquée"
            Il s’agit de remplacer 𝑥 par la valeur donnée.  
            Se rappeler que 3𝑥 = 3 × 𝑥 (on refait apparaître la multiplication entre un nombre et une lettre).  

            ^^Exemple 1^^ : Calcule 3𝑥 + 2 pour 𝑥 = 2.  
            3 × 2 + 2 = 6 + 2 = **8**  

            ^^Exemple 2^^ : Calcule 5 + 𝑥 + 4 pour 𝑥 = 4.  
            5 + 4 + 4 = **13**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NE3~o=0~q=0.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Appliquer une formule (substitution)](https://www.youtube.com/watch?v=FOSVfFdDi7w){:target="_blank"}

    === ":blue_square: Bleue"
        ??? question "1) Triangle rectangle ou non ?"
            Pour montrer qu'un triangle est rectangle :  
            - on peut montrer qu'il y a un angle droit.  
            - on peut tester l'égalité de Pythagore.  

            ^^Exemple 1^^ : ABC un triangle tel que AB = 4 cm, BC = 5 cm et AC = 3cm.  
            5² = 25  
            4² + 3² = 16 + 9 = 25  
            Donc le triangle ABC est rectangle en A.  

            ^^Exemple 2^^ : DEF un triangle tel que DE = 6 cm, DF = 2 cm et EF = 4 cm.  
            6² = 36  
            2² + 4² = 4 + 16 = 20  
            Donc le triangle n’est pas rectangle.  

            !!! quote ""
                :material-weight-lifter: Genially : [6 questions](https://view.genial.ly/60759dbad4b60e0df6a964d4){:target="_blank"}  

                :octicons-video-16: [Appliquer l'égalité de Pythagore](https://www.youtube.com/watch?v=puXyHcU5Awg){:target="_blank"} 

        ??? question "2) Multiplication par une puissance de 10"
            La puissance de 10 indique de combien de rang on doit "décaler" la virgule.  
            Si la puissance est positive, on décale la virgule vers la gauche (le nombre deviendra plus grand).  
            Si la puissance est négative, on décale la virgule vers la droite (le nombre deviendra plus petit).  

            ^^Exemple 1^^ : 7,3 × 10² = 730  

            ^^Exemple 2^^ : 17 × $10^{-2}$ =  0,17  

            !!! quote ""
                :material-weight-lifter: JPPJM : [15 secondes](www.jepeuxpasjaimaths.fr/?ex=puissancesde10cycle4){:target="_blank"}  

                :material-weight-lifter: Maths Mentales : [préfixes - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=4NA8~o=0,1,2,3~q=0.-1.-2.-3.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Écrire sous forme décimale un nombre écrit avec des puissances de 10](https://www.youtube.com/watch?v=vRPOgw3Sfnk){:target="_blank"}

        ??? question "3) Calcul d’une distance ou d’un temps"
            Il faut avoir en tête que la vitesse c’est une distance divisée par un temps.  
            Pour calculer une distance : on multiplie la vitesse par le temps.  
            Pour calculer un temps : on divise la distance par la vitesse.  

            ^^Exemple 1^^ : v = 12 km/h , d = 6 km   
            donc t = $\dfrac{6}{12}$ = 0,5 h ou 30 min  

            ^^Exemple 2^^ : v = 20 m/s , t = 10 s  
            donc d = 20 × 10 = 200 m  

            ^^Exemple 3^^ : v = 4,5 km/h , d = 13,5 km  
            donc t = $\dfrac{13,5}{4,5}$ = 3 h  
 
            !!! quote ""
                :material-weight-lifter: Genially : [5 questions](https://view.genial.ly/5fd92a893c971e0d6598bd00){:target="_blank"}  

                :octicons-video-16: [Effectuer des calculs de vitesse](https://www.youtube.com/watch?v=1t6fCpwVT6o){:target="_blank"}

        ??? question "4) Opérations avec les nombres relatifs"
            Voir les précédentes ceintures (additions et soustractions, multiplication et divisions).  

            !!! quote ""
                :material-weight-lifter: JPPJM : [15 secondes](www.jepeuxpasjaimaths.fr/?ex=melimelocycle4){:target="_blank"}  

                :octicons-video-16: [Effectuer des calculs avec les relatifs](https://www.youtube.com/watch?v=O3mI59OVnJI){:target="_blank"}

        ??? question "5) Multiplication de fractions"
            On multiplie les numérateurs ensemble et les dénominateurs ensemble, tout en simplifiant ce qui peut l’être à l’aide de décomposition.  

            ^^Exemple 1^^ : $\dfrac{10}{7} × \dfrac{3}{25} = \dfrac{2×5×3}{7×5×5} = \dfrac{6}{35}$  

            ^^Exemple 2^^ : $\dfrac{15}{22} × \dfrac{4}{35} = \dfrac{3×5×2×2}{2×11×7×5} = \dfrac{6}{77}$  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 30 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=4NC3~o=0~q=0.~p=~t=30~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer des multiplications de fractions](https://www.youtube.com/watch?v=j27kXXrw3Xk){:target="_blank"}

        ??? question "6) Développer une expression"
            Développer, c’est passer d’un produit à une somme. On utilise la propriété de distributivié :  
            k × (a + b) = k × a + k × b  

            ^^Exemple 1^^ : 3 × (𝑥 + 4) = 3×𝑥 + 3×4 = 3𝑥 + 12  

            ^^Exemple 2^^ : 5 × (𝑦 − 2) = 5×𝑦 − 5×2 = 5𝑦 − 10  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=4NE4~o=1,0~q=0.0,1-1.0,1~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Développer une expression](https://www.youtube.com/watch?v=7k5kFah3z7w){:target="_blank"}

    === ":brown_square: Marron"
        ??? question "1) Factoriser une expression littérale"
            Factoriser une expression, c’est transformer une somme (ou une différence) en un produit. C'est la transformation inverse du développement.  
            k × a + k × b = k × (a + b)  

            ^^Exemple 1^^ : 5𝑥 + 10 = 5 × 𝑥 + 5 × 2 = 5 × (𝑥 + 2)  

            ^^Exemple 2^^ : 4𝑦 − 8 = 4 × 𝑦 − 4 × 2 = 4 × (𝑦 − 2)  

            ^^Exemple 3^^ : 12𝑥 + 6𝑥² = 6𝑥 × 2 + 6𝑥 × 𝑥 = 6𝑥 × (2 + 𝑥)  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=4NE5~o=0,1,2,3~q=0.-1.-2.-3.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Factoriser une expression](https://www.youtube.com/watch?v=8NDOC54YLzg){:target="_blank"}  

        ??? question "2) Notation scientifique"
            Il s’agit d’écrire un nombre sous la forme $𝑎 × 10^n$ avec 𝑎 compris entre 1 et 10 exclu et 𝑛 un nombre entier.  
            **Méthode** : Le chiffre des unités a la valeur désignée par la puissance de 10. Il reste à placer la virgule au bon endroit.  

            ^^Exemple^^ : 6,22 × $10^{-3}$ = 0,00622 car 6 est le chiffre des millièmes ($10^{-3}$)  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=4NA1~o=0,1~q=0.-1.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Ecrire un nombre sous forme scientifique](https://www.youtube.com/watch?v=tzhNCpLRtCY){:target="_blank"}

        ??? question "3) Conversion aires et volumes"
            Pour convertir des unités d'**aire** : km² - hm² - dam² - m² - dm² - cm² - mm²  
            Le tableau des mesures d'unités d'aire comprend **2** colonnes par unité.  
            1 m² = 1 m × 1 m = 10 dm × 10 dm = 100 dm²  

            ^^Exemple 1^^ : 158,93 m² = 15 893 dm²  
            95,4 cm² = 0,954 dm²  
            167,3 hm² = 1,673 km²  

            Pour convertir des unités de **volume** : km³ - hm³ - dam³ - m³ - dm³ - cm³ - mm³  
            1 dm³ = 1 L
            Le tableau des mesures d'unités de volume comprend **3** colonnes par unité.  
            1 m³ = 1 m × 1 m × 1 m = 10 dm × 10 dm × 10 dm = 1 000 dm³  

            ^^Exemple 2^^ : 50 cm³ = 0,05 dm³  
            8 L = 8 dm³ = 0,008 m³  
            25 m³ = 25 000 dm³ = 25 000 L  
            103,6 m³ = 0,1036 dam³  

            !!! quote ""
                :material-weight-lifter: Genially : [aires - 8 questions](https://view.genial.ly/60769e8c54eed30d321d3d94){:target="_blank"}  

                :material-weight-lifter: Genially : [volumes - 8 questions](https://view.genial.ly/6076a1f748e9c30db23c6ab0){:target="_blank"}  

                :octicons-video-16: [Convertir les unités d'aire](https://www.youtube.com/watch?v=qkDy6lguF80){:target="_blank"}  

                :octicons-video-16: [Convertir les unités de volume](https://www.youtube.com/watch?v=nnXfRWe4WDE){:target="_blank"}  

        ??? question "4) Réduire une expression littérale"
            Réduire une expression littérale, c’est l’écrire avec le moins de termes possibles. On regroupe tous les membres **d’une même famille** ensemble.  

            ^^Exemple 1^^ : 3𝑥 + 4 + 5𝑥 = 4 + 8𝑥 (il y a les termes en 𝑥 ensemble et le nombre seul)  

            ^^Exemple 2^^ : 5𝑥² + 3𝑥 = 5𝑥² + 3𝑥 (car 𝑥 et 𝑥² ne sont pas de la même famille)  

            ^^Exemple 3^^ : 7 × 8𝑥 = 56𝑥 (on peut multiplier les nombres entre-eux, c'est ici de la simplification)  

            ^^Exemple 4^^ : 3𝑥 + 𝑥 = 4𝑥    

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NE6~o=0~q=0.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Réduire une expression littérale](https://www.youtube.com/watch?v=bgpDnvvgBlM){:target="_blank"}

        ??? question "5) Division de fractions"
            Pour diviser deux fractions, on multiplie la première fraction avec l’inverse de la deuxième fraction.  
            $\dfrac{a}{b} ÷ \dfrac{c}{d} = \dfrac{a}{b} × \dfrac{d}{c}$ avec b, c et d non nuls  

            ^^Exemple^^ : $\dfrac{5}{4} ÷ \dfrac{9}{7} = \dfrac{5}{4} × \dfrac{7}{9} = \dfrac{5×7}{4×9} = \dfrac{35}{36}$  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [4 questions - 40 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=4NC4~o=0~q=0.0~p=~t=40~n=4){:target="_blank"}  

                :octicons-video-16: [Effectuer des divisions de fractions](https://www.youtube.com/watch?v=7_hZWOoMBSA){:target="_blank"}

        ??? question "6) Double distributivité"
            (a + b)(c + d) = a × c + a × d + b × c + b × d  

            ^^Exemple 1^^ : (𝑥+2)(𝑥+3) = 𝑥×𝑥 + 𝑥×3 + 2×𝑥 + 2×3 = 𝑥² + 3𝑥 + 2𝑥 + 6 = 𝑥² + 5𝑥 + 6  
            ^^Exemple 2^^ : (3𝑥+4)(𝑥−2) = 3𝑥×𝑥 + 3𝑥×(−2) + 4×𝑥 + 4×(−2) = 3𝑥² − 6𝑥 + 4𝑥 − 8 = 3𝑥² − 2𝑥 − 8  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [4 questions - 40 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=4NE4~o=2,3,4~q=2.-3.-4.~p=~t=40~n=4){:target="_blank"}  

                :octicons-video-16: [Développer en utilisant la double distributivité](https://www.youtube.com/watch?v=YS-3JI_z2f0){:target="_blank"}

    === ":black_large_square: Noire"
        ??? question "1) Calcul d’un prix après réduction"
            On calcule d'abord le montant de la réduction, puis on trouve le prix final :  
            Prix final = Prix initial – réduction  

            ^^Exemple^^ : Réduction de 25 % de 40 €.  
            25 % de 40 €, c’est le quart de 40 € donc 40 ÷ 4 = 10 €  
            La réduction est de 10 € donc le prix final est de 40 − 10 = 30 €.  

            !!! quote ""
                :material-weight-lifter: Genially : [8 questions](https://view.genial.ly/60706bd398b2300d7f232df7){:target="_blank"}  

                :octicons-video-16: [Appliquer un pourcentage](https://www.youtube.com/watch?v=2UVaPRdSMl0){:target="_blank"}  

        ??? question "2) Résoudre une équation"
            Résoudre une équation, c’est trouver la valeur de l’inconnue pour laquelle l’égalité est vraie.  
            Pour isoler 𝑥, on ajoute son opposé de chaque côté de l’égalité.  

            ^^Exemple 1^^ : 𝑥 − 8 = 3 devient 𝑥 − 8 + 𝟖 = 3 + 𝟖 et donc 𝑥 = 11  

            ^^Exemple 2^^ : 𝑥 + 5 = −6 devient 𝑥 + 5 − 𝟓 = − 6 −𝟓 et donc 𝑥 = −11  
 
            On peut aussi multiplier ou diviser de chaque côté de l’égalité.  

            ^^Exemple 3^^ : 3𝑥 = 5 donc 𝑥 = $\dfrac{5}{3}$  

            ^^Exemple 4^^ : 4𝑥 = 6 donc 𝑥 = $\dfrac{6}{4}$ = 1,5  

            Et pour les équations plus complexes, on combine les 2 méthodes.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [4 questions - 40 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=4NE9~o=0,1~q=0.-1.-2.~p=~t=40~n=4){:target="_blank"}  

                :octicons-video-16: [Résoudre une équation](https://www.youtube.com/watch?v=9rCgwGqJv6I){:target="_blank"}

        ??? question "3) Fractions irréductibles"
            Rendre irréductible une fraction, c’est l’écrire sous la forme la plus simple possible et donc avec le numérateur et le dénominateur les plus petits possibles. Pour cela, on décompose le numérateur et le dénominateur.  

            ^^Exemple 1^^ : $\dfrac{24}{9} = \dfrac{3×8}{3×3} = \dfrac{8}{3}$

            Il est possible de simplifier en faisant plusieurs étapes.  
            
            ^^Exemple 2^^ : $\dfrac{60}{15} = \dfrac{5×12}{5×3} = \dfrac{12}{3} = \dfrac{3×4}{3×1} = \dfrac{4}{1} = 4$

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 30 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6NB5~o=1~q=1.~p=~t=30~n=6){:target="_blank"}  

                :octicons-video-16: [Simplifier une fraction](https://www.youtube.com/watch?v=6ce96Tze9nI){:target="_blank"}

        ??? question "4) Test d’égalité"
            Tester une égalité, c’est remplacer la lettre par une valeur qui est donnée et ensuite regarder si l’égalité est vraie ou non.  
            **Méthode** : On calcule **séparément** les 2 membres de l'égalité puis on compare.  
        
            ^^Exemple 1^^ : 3𝑥 = 𝑥 + 2 pour 𝑥 = 2 ?  
            • 3𝑥 = 3 × 2 = 6  
            • 𝑥 + 2 = 2 + 2 = 4  
               6 ≠ 4 donc cette égalité est fausse pour 𝑥 = 2.  
 
            ^^Exemple 2^^ : 4𝑥 = 3𝑥 + 1 pour 𝑥 = 1 ?  
            • 4𝑥 = 4 × 1 = 4  
            • 3𝑥 + 1 = 3 × 1 + 1 = 3 + 1 = 4  
            4 = 4 donc cette égalité est vraie pour 𝑥 = 1.  

            !!! quote ""
                :material-weight-lifter: Genially : [9 questions](https://view.genial.ly/60706bed6f2a060d1f43d0fe){:target="_blank"}  

                :material-weight-lifter: Maths Mentales : [4 questions - 40 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NE4~o=0~q=0.~p=~t=40~n=4){:target="_blank"}  

                :octicons-video-16: [Vérifier si un nombre est solution d'une équation](https://www.youtube.com/watch?v=PLuSPM6rJKI){:target="_blank"}

        ??? question "5) Calcul astucieux avec les relatifs"
            On combine les règles de calcul avec les nombres relatifs et les priorités de calcul.  

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/6024ed4889b00a0d2757e87b){:target="_blank"}  

        ??? question "6) Appliquer des produits en croix"
            ^^Exemple 1^^ :  

            | 4 | 10 |
            |:-:|:-: |
            | 5 | ?  |

            ? = $\dfrac{5×10}{4} = \dfrac{50}{4} = 12,5$  

            ^^Exemple 2^^ :  

            | 6 | ?  |
            |:-:|:-: |
            | 3 | 10 |

            ? = $\dfrac{6×10}{3} = \dfrac{60}{3} = 20$  



<!-- Objectifs du livret CM -->

:dart: **Les objectifs des ceintures CM :**  
- Meilleure maîtrise des tables de multiplication  
- Meilleure maîtrise des méthodes simples de calcul mental  
- Travailler en temps limité  
- Développer des automatismes  
- Apprendre la logique  
- Apprendre à raisonner  
- Apprendre à travailler en autonomie  

:material-weight-lifter: Les **efforts** que tu fourniras tout au long de l'année pour réussir chacune de ces ceintures t'amèneront à avoir plus de connaissances et plus de facilités à travailler.  

![](../images/logo_cbpm.png){ width=5% }
