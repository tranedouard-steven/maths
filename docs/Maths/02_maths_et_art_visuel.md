---
author: ET
title: 🎨 Maths et art visuel
hide:
  - footer
---

La relation entre les mathématiques et les **arts** s’inscrit dans l’**histoire**. Les artistes ont pu profiter au fil du temps de différents outils mathématiques.  

## Exemples
??? note "Exemples d’outils exploités par les créateurs d’hier et d’aujourd’hui"
    :fontawesome-regular-file-pdf: [La perspective](https://ecolebranchee.com/wp-content/uploads/2017/04/Perspective.pdf){:target="_blank"}  
    :fontawesome-regular-file-pdf: [Les lignes de composition](https://ecolebranchee.com/wp-content/uploads/2017/04/LignesdeComposition.pdf){:target="_blank"}  
    :fontawesome-regular-file-pdf: [Les proportions et le nombre d’or](https://ecolebranchee.com/wp-content/uploads/2017/04/Proportions_nombre_dor.pdf){:target="_blank"}  
    :fontawesome-regular-file-pdf: [La symétrie et l’asymétrie](https://ecolebranchee.com/wp-content/uploads/2017/04/Symetrie.pdf){:target="_blank"}  
    :fontawesome-regular-file-pdf: [Les transformations géométriques](https://ecolebranchee.com/wp-content/uploads/2017/04/Transformations_geometriques.pdf){:target="_blank"}  
    :fontawesome-regular-file-pdf: [Les algorithmes et les formules mathématiques](https://ecolebranchee.com/wp-content/uploads/2017/04/Algorithmes.pdf){:target="_blank"}  
    :fontawesome-regular-file-pdf: [Les stratégies de classement](https://ecolebranchee.com/wp-content/uploads/2017/04/Strategies_de_classement.pdf){:target="_blank"}  
    :fontawesome-regular-file-pdf: [Dossier architecture et mathématiques](https://thierrymachuron.typepad.com/files/archi-science-light.pdf){:target="_blank"}  


## L'illusion d'optique
??? note "Exemples d’illusions d'optique"
    === "⁉️"
        :fontawesome-regular-file-pdf: [C'est quoi une illusion d'optique ?](https://nuage03.apps.education.fr/index.php/s/F6WYk3bjssSiZPz){:target="_blank"}  
    
    === "Ex1"
        ![](../images/illusion_1.gif)  
        Les serpents tournent spontanément.

    === "Ex2"
        ![](../images/illusion_2.jpg)  
        Les rouleaux semblent tourner sans effort.  

    === "Ex3"
        ![](../images/illusion_3.gif)  
        L'anneau extérieur de rayons semble tourner dans le sens des aiguilles d'une montre tandis que l'anneau intérieur tourne dans le sens inverse des aiguilles d'une montre.

    === "Ex4"
        ![](../images/illusion_4.jpg)  
        Ce fond à carreaux se compose de carrés mais semble onduler. 

    === "Ex5"
        D'autres illusions à découvrir en [cliquant ici](https://www.ritsumei.ac.jp/~akitaoka/index-e.html){:target="_blank"}.  

        :octicons-video-16: [Quels sont les secrets des illusions d’optique ?](https://www.dailymotion.com/video/x1i14uu){:target="_blank"} 



## L'anamorphose
??? note "Exemples d’anamorphoses"
    === "⁉️"
        L'**anamorphose** est une particularité étonnante de la perspective.  
        Une anamorphose est une déformation d'une image par allongement ou à l'aide d'un système optique, tel un miroir courbe. Certains artistes ont produit des œuvres par ce procédé et ainsi, ont créé des œuvres déformées qui se recomposent selon un point de vue privilégié. L'anamorphose est une sorte d'illusion d'optique.
    
    === "Vidéo"
        :octicons-video-16: [Le dessin de crocodile est d'un réalisme saisissant.](https://www.dailymotion.com/video/x88xhab){:target="_blank"} 

    === "Street Art"
        ![](../images/illusion_5.jpg)  

    === "Robert-Ortis"
        **Matthieu Robert-Ortis** est un sculpteur français, réalisant de magnifiques œuvres qui changent de visage selon l’angle sous lequel on les regarde.  
        :octicons-video-16: [Epigénétique](https://ladigitale.dev/digiview/#/v/663e5e291e21e){:target="_blank"}  
        :octicons-video-16: [Zingst](https://ladigitale.dev/digiview/#/v/663e5e3bc7940){:target="_blank"}  
        :octicons-video-16: [Le Capitalocène à l'ère du Verseau](https://ladigitale.dev/digiview/#/v/663e5e51b68df){:target="_blank"}  
        :octicons-video-16: [L'Homme-Crabe](https://ladigitale.dev/digiview/#/v/663e5e645dfe2){:target="_blank"}  
        :octicons-video-16: [Éléphant-Girafes](https://ladigitale.dev/digiview/#/v/663e5e7a58a7f){:target="_blank"}  
        :octicons-video-16: [King of Neptune](https://ladigitale.dev/digiview/#/v/663e5ebe08add){:target="_blank"}  

    === "Abélanet"
        :octicons-video-16: [Anamorphose avec François Abélanet](https://www.youtube.com/watch?v=7m2YiEM1Y3M){:target="_blank"}
        


## Présentation d'artistes
??? note "Découverte d'artistes"
    === "Rosanes"
        **Kerby Rosanes** est un illustrateur philippin qui travaille essentiellement au crayon et feutre noir.  
        L’illustrateur a créé une série d’animaux géométriques justement intitulée Geometric Beasts.  
        ![](../images/kerby_rosanes.jpg)  
        Plus d'illustrations à découvrir sur son [site internet](https://kerbyrosanes.com/geometric-beasts){:target="_blank"}.

    === "Nihalani"
        Artiste new-yorkais, **Aakash Nihalani** propose une vision alternative des paysages urbains. Ses œuvres de rues sont des trompes l’œil et des illusions d’optiques composés de formes géométriques qui créent des décors fantasques et colorés. Armé non pas de pinceaux ni de bombes, mais de rubans adhésifs fluorescents, Aakash Nihalani pose une note de couleur lumineuse et brillante sur les mobiliers urbains, les pavés ou les murs d’immeubles.  
        ![](../images/aakash_nihalani.jpg)  
        Plus de photos (outdoor et indoor) à découvrir sur son [site internet](https://www.aakashnihalani.com/outdoor){:target="_blank"}.  

        

        




