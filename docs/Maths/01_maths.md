---
author: ET
title: ❗ Les maths sont partout
hide:
  - footer
---

:octicons-video-16: [Spiderman et les maths :fontawesome-regular-face-grin-wink:](https://ladigitale.dev/digiview/#/v/64db8af76d6b4){:target="_blank"}  

??? note "À quoi ça sert les maths ?"
    :octicons-video-16:  1 jour 1 question : 
    <iframe src="https://ladigitale.dev/digiview/inc/video.php?videoId=LEmnkuYzHME&vignette=https://i.ytimg.com/vi/LEmnkuYzHME/hqdefault.jpg&debut=0&fin=103&largeur=200&hauteur=113" allowfullscreen frameborder="0" width="640" height="360"></iframe>  

    :octicons-video-16: C'est toujours pas sorcier : [Les maths, naturellement géniales](https://www.france.tv/france-4/c-est-toujours-pas-sorcier/saison-5/4345282-les-maths-naturellement-geniales.html){:target="_blank"}  

    :globe_with_meridians: [Les maths sont partout](https://everywhere.idm314.org/index_fr.html){:target="_blank"} 

    :globe_with_meridians: [Les maths pour un monde meilleur](https://betterworld.idm314.org/index_fr.html){:target="_blank"}  

    :fontawesome-regular-file-pdf: [Les maths de A à Z](https://nuage03.apps.education.fr/index.php/s/5X5ca7DEK46XtdB){:target="_blank"}  


??? note "Les vidéos du CNRS (Centre national de la recherche scientifique)"
    :octicons-video-16: [Les maths sont partout](https://ladigitale.dev/digiview/#/v/66256325f3e9a){:target="_blank"}  

    :octicons-video-16: [Les maths, une opportunité pour les jeunes](https://ladigitale.dev/digiview/#/v/662563a4d672c){:target="_blank"}  

    :octicons-video-16: [Les maths, un outil, un langage](https://ladigitale.dev/digiview/#/v/6625644536b8e){:target="_blank"}  

    :octicons-video-16: [La citoyenneté des maths](https://ladigitale.dev/digiview/#/v/6625646ada571){:target="_blank"}  

    :octicons-video-16: [Les maths, une manière de construire sa pensée](https://ladigitale.dev/digiview/#/v/662564a495074){:target="_blank"}  


??? note "À la découverte de mathématiciens et mathématiciennes"
    |![](../images/mathematiciens/01_David_Hilbert.jpg)|[David Hilbert](https://ladigitale.dev/digiview/#/v/66295d62edac6){:target="_blank"}|
    |:-:|:--|
    |![](../images/mathematiciens/02_Leonhard_Euler.png)|[Leonhard Euler](https://ladigitale.dev/digiview/#/v/66295d492db39){:target="_blank"}|
    |![](../images/mathematiciens/03_Srinivasa_Ramanujan.png)|[Srinivasa Ramanujan](https://ladigitale.dev/digiview/#/v/66295d8c2a18f){:target="_blank"}|
    |![](../images/mathematiciens/04_Sofia_Kovalevskaya.png)|[Sofia Kovalevskaya](https://ladigitale.dev/digiview/#/v/66295daa58a3b){:target="_blank"}|
    |![](../images/mathematiciens/05_Kunihiko_Kodaira.jpg)|[Kunihiko Kodaira](https://ladigitale.dev/digiview/#/v/66295dcd43363){:target="_blank"}|
    |![](../images/mathematiciens/06_Hypatie.jpg)|[Hypatie](https://ladigitale.dev/digiview/#/v/66295e560ea5a){:target="_blank"}|
    |![](../images/mathematiciens/07_Andreï_Kolmogorov.jpg)|[Andreï Kolmogorov](https://ladigitale.dev/digiview/#/v/66295e7fa99c7){:target="_blank"}|
    |![](../images/mathematiciens/08_John_Nash.jpg)|[John Nash](https://ladigitale.dev/digiview/#/v/66295ea2e72f7){:target="_blank"}|
    |![](../images/mathematiciens/09_Marjorie_Rice.jpg)|[Marjorie Rice](https://ladigitale.dev/digiview/#/v/66295ec797bb3){:target="_blank"}|
    |![](../images/mathematiciens/10_Ada_Lovelace.jpg)|[Ada Lovelace](https://ladigitale.dev/digiview/#/v/66295f046138f){:target="_blank"}|
    |![](../images/mathematiciens/11_Alan_Turing.jpg)|[Alan Turing](https://ladigitale.dev/digiview/#/v/66295f2587c18){:target="_blank"}|
    |![](../images/mathematiciens/12_Asuman_Ozdaglar.jpg)|[Asuman Ozdaglar](https://ladigitale.dev/digiview/#/v/66295f5505293){:target="_blank"}|
    |![](../images/mathematiciens/13_Pierre_de_Fermat.png)|[Pierre de Fermat](https://ladigitale.dev/digiview/#/v/66295f71caf89){:target="_blank"}|
    |![](../images/mathematiciens/14_Jacques_Bernoulli.jpg)|[Jacques Bernoulli](https://ladigitale.dev/digiview/#/v/66295f99df154){:target="_blank"}|
    |![](../images/mathematiciens/15_Maryna_Viazovska.png)|[Maryna Viazovska](https://ladigitale.dev/digiview/#/v/66295fbd3ec05){:target="_blank"}|
    |![](../images/mathematiciens/16_Évariste_Galois.jpg)|[Évariste Galois](https://ladigitale.dev/digiview/#/v/66295fe1aa577){:target="_blank"}|
    |![](../images/mathematiciens/17_Alexandre_Grothendieck.jpg)|[Alexandre Grothendieck](https://ladigitale.dev/digiview/#/v/662960003bd6d){:target="_blank"}|
    |![](../images/mathematiciens/18_Brahmagupta.jpg)|[Brahmagupta](https://ladigitale.dev/digiview/#/v/662960215621f){:target="_blank"}|
    |![](../images/mathematiciens/19_Johannes_Kepler.jpg)|[Johannes Kepler](https://ladigitale.dev/digiview/#/v/6629603bb80a8){:target="_blank"}|
    |![](../images/mathematiciens/20_Logo.jpg)|[Bilan en chanson !](https://ladigitale.dev/digiview/#/v/6629608011cc5){:target="_blank"}|


!!! abstract "Dictionnaire"
    :globe_with_meridians: [Le dictionnaire des maths au collège](https://tacproedupublicresources.blob.core.windows.net/hatier/dicomaths/index.html){:target="_blank"}  

    :fontawesome-regular-file-pdf: [Le vocabulaire des consignes en maths](https://nuage03.apps.education.fr/index.php/s/2BzkaBsFGmtdqcS){:target="_blank"}  
