---
author: ET
title: 🧠 Apprendre de manière efficace
hide:
  - footer
---

??? note "Développe une mentalité de croissance."
    Change tes **mots**, change ton **état d'esprit** !  
    
    ![Mentalité de croissance](../images/mentalité_de_croissance.jpg){ width=100% }


??? note "Découvre 3 stratégies pour t'aider à mieux apprendre."
    === ":octicons-video-16:"
        <iframe src="https://ladigitale.dev/digiview/inc/video.php?videoId=tfXcUfP81w4&vignette=https://i.ytimg.com/vi/tfXcUfP81w4/hqdefault.jpg&debut=0&fin=274&largeur=200&hauteur=113" allowfullscreen frameborder="0" width="640" height="360"></iframe>
    
    === "La mémoire"
        :fontawesome-regular-file-pdf: [Comment fonctionne la mémoire ?](https://nuage03.apps.education.fr/index.php/s/52JWwkqScyXFJG3){:target="_blank"}

    === "Plus de vidéos"
        Découvre d'autres vidéos produites par le groupe académique des sciences cognitives d'Aix-Marseille.  

        01. [Qu’a-t-on dans la tête ?](https://ladigitale.dev/digiview/#/v/6776e19048ce0){:target="_blank"}  

        02. [A quoi sert le cerveau ?](https://ladigitale.dev/digiview/#/v/6776ec72085b0){:target="_blank"}  

        03. [Les neurones](https://ladigitale.dev/digiview/#/v/6776ec9d8faaa){:target="_blank"}  

        04. [Comment voir dans la tête ?](https://ladigitale.dev/digiview/#/v/6776ece2ee4b7){:target="_blank"}  

        05. [Comment apprend-on ?](https://ladigitale.dev/digiview/#/v/6776ed01b53c2){:target="_blank"}  

        06. [L'erreur](https://ladigitale.dev/digiview/#/v/6776ed1b826c1){:target="_blank"}  

        07. [La plasticité cérébrale](https://ladigitale.dev/digiview/#/v/6776ed40f1fa8){:target="_blank"}  

        08. [Les écrans et leur utilisation](https://ladigitale.dev/digiview/#/v/6776ed652f448){:target="_blank"}  

        09. [De quoi a besoin le cerveau ?](https://ladigitale.dev/digiview/#/v/6776ed83d18d4){:target="_blank"}  

        10. [La mémorisation](https://ladigitale.dev/digiview/#/v/6776eda643027){:target="_blank"}   


??? note "Crée une synthèse efficace de la leçon." 
    === "Fiche de mémorisation"
        - À **gauche** : une **question** simple et précise  
        - À **droite** : la **réponse** que l'on doit retenir  

        Ce qui importe est de pouvoir **cacher la réponse** pendant que l'on se pose la question.  

        Les objectifs se résument en 4 points :  
        1. **Mobiliser la mémorisation sur les essentiels**. La fiche de mémorisation met le cap sur l’essentiel, « ce qui sera fondamental de retenir » pour comprendre les situations à venir, traiter les tâches, avancer efficacement.  
        2. **Pratiquer la mémorisation active** : la fiche est clairement séparée en deux parties, les questions d’une part, les réponses de l’autre. L’apprenant est autonome, il peut se poser lui-même les questions, il n’a pas besoin d’autrui.  
        3. **Après avoir réalisé l’effort de mémorisation**, de recherche de la solution dans ses souvenirs, l’apprenant consulte la réponse et peut rectifier son erreur, lever le malentendu, obtenir la bonne réponse : c’est le **feedback proche**.  
        4. Enfin, l’apprenant pourra réviser rapidement ses questions grâce à cette méthode active et efficace, aux moments opportuns de réactivation, selon le principe du **réapprentissage expansé dans le temps**.  

        ![](../images/memo_fiche_exemple.png) 

    === "Carte mentale"
        ![](../images/memo_carte_mentale1.jpg)  
        ![](../images/memo_carte_mentale2.jpg)  
        <iframe src="https://player.vimeo.com/video/171395224?h=6485cc3e05&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

    === "Flashcards"
        :fontawesome-regular-file-pdf: [Flashcards pour réviser](https://nuage03.apps.education.fr/index.php/s/Lo2S8g32Rd3QLqi){:target="_blank"}  

        ![](../images/memo_flashcards.jpg) 

    === "Sketchnote"
        :fontawesome-regular-file-pdf: [Skechnote ton cours](https://nuage03.apps.education.fr/index.php/s/2K6HEqkEjdn8FCf){:target="_blank"}  

        ![](../images/memo_sketchnote.jpg) 

 