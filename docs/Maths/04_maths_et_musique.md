---
author: ET
title: 🎵 Maths et musique
hide:
  - footer
---

:octicons-video-16: [Les mathématiques de la musique](https://ladigitale.dev/digiview/#/v/6626180f57734){:target="_blank"}  

:octicons-video-16: [Musique et mathématiques - Histoire d'une rencontre](https://ladigitale.dev/digiview/#/v/6626183fb16e7){:target="_blank"}  

![](../images/logo_genially.png) [Rencontre entre musique et mathématiques](https://view.genial.ly/628134e409d31000189a5cc5){:target="_blank"}  

:octicons-video-16: [Pythagore et "l'art de faire entendre les nombres"](https://ladigitale.dev/digiview/#/v/669a54b599d45){:target="_blank"}  
