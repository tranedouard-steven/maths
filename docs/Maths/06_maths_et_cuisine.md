---
author: ET
title: 🧁 Maths et cuisine
hide:
  - footer
---

:octicons-video-16: [Maths & cuisine : Fibonacci et le nombre d'or](https://ladigitale.dev/digiview/#/v/663df983d5593){:target="_blank"}  

:globe_with_meridians: [Comment les maths sont utilisées dans la cuisine](https://parlonssciences.ca/ressources-pedagogiques/documents-dinformation/cuisiner-les-mathematiques){:target="_blank"}  