---
author: ET
title: 🎮 Maths et jeux vidéo
hide:
  - footer
---

Films d’animation, jeux vidéo et autres créations 3D : derrière les univers virtuels, de plus en plus réalistes, se cachent bien souvent… des maths !

:octicons-video-16: [Les maths derrière les jeux vidéo #1 : Éclairage](https://ladigitale.dev/digiview/#/v/662618d8a77c2){:target="_blank"}  

:octicons-video-16: [Les maths derrière les jeux vidéo #2 : Illusions d'optique et ombres](https://ladigitale.dev/digiview/#/v/662618f60e844){:target="_blank"}  

:octicons-video-16: [Les maths derrière les jeux vidéo #3 : Surfaces développables](https://ladigitale.dev/digiview/#/v/662619223315c){:target="_blank"}  

:octicons-video-16: [Les maths derrière les jeux vidéo #4 : Finesse du maillage](https://ladigitale.dev/digiview/#/v/6626193dd81ba){:target="_blank"}  

:octicons-video-16: [Les maths derrière les jeux vidéo #5 : S'amuser avec les polyèdres](https://ladigitale.dev/digiview/#/v/6626195e4ddd6){:target="_blank"}  

:octicons-video-16: [Les maths derrière les jeux vidéo #6 : Drôle de surfaces !](https://ladigitale.dev/digiview/#/v/66261974cfff0){:target="_blank"}  
