---
author: ET
title: 🌳 Maths et nature
hide:
  - footer
---

:octicons-video-16: [La nature fait des maths](https://ladigitale.dev/digiview/#/v/66261783efecb){:target="_blank"}  

:fontawesome-regular-file-pdf: [Les maths au service du développement durable](https://nuage03.apps.education.fr/index.php/s/5rSFNpfWsrtp3jq){:target="_blank"}  