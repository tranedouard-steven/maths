---
author: ET
title: 📓 Leçons 3e
hide:
  - footer
---

??? note "01 Le calcul numérique"
    :construction:

??? note "02 L'égalité de Pythagore"
    !!! danger ""
        === "Je révise"
            📄 Fiche de mémorisation : [Wooflash](https://app.wooflash.com/join/OH94TOCF?from=1){:target="_blank"}  
            🗃️ Carte mentale : [BaREM](https://nuage03.apps.education.fr/index.php/s/RCy3GeKBYZnjLrb){:target="_blank"} ([+](https://www.hatier-clic.fr/miniliens/mie/9782401077331/BaREM_carte_anim_D04/index.html){:target="_blank"}) - [Myriade](https://nuage03.apps.education.fr/index.php/s/apJTDQAXp2dHTK7){:target="_blank"}  
            ☑️ QCM : [Myriade](https://applets.directplateforme.com/Bordas/bbe/9782047338186/733818_M4_09_qcm/index.html){:target="_blank"}  
            🎮 Genially : [Pythagore dont tu es le héros](https://view.genial.ly/613e55c17602e60d61414c2a){:target="_blank"}

        === "J'approfondis"
            :octicons-video-16: Petits contes mathématiques : [C'est quoi le théorème de Pythagore ?](https://www.lumni.fr/video/petits-contes-mathematiques-le-theoreme-de-pythagore#containerType=program&containerSlug=petits-contes-mathematiques){:target="_blank"}  
            :octicons-video-16: Petits contes mathématiques : [C'est quoi la racine carrée ?](https://www.lumni.fr/video/petits-contes-mathematiques-la-racine-carree#containerType=program&containerSlug=petits-contes-mathematiques){:target="_blank"}  

    ### **1) Connaître le vocabulaire dans le triangle rectangle**
    > :one: Quels noms donne-t-on aux côtés d’un triangle rectangle ?   

    ### **2) Connaître le théorème de Pythagore**
    > :two: Peux-tu énoncer le théorème de Pythagore ?  

    :mag_right: [Illustration du théorème de Pythagore](https://www.youtube.com/watch?v=Uh4Z9xfyx_k){:target="_blank"}  
    ![](../images/logo_coopmaths.png) [S'entraîner à donner une égalité de Pythagore](https://coopmaths.fr/alea/?uuid=40c47&id=4G20-1&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  

    ### **3) Montrer qu’un triangle est rectangle ou non**
    > :three: Comment montrer qu’un triangle est rectangle ou non, quand on connaît la longueur de ses 3 côtés ?  

    :octicons-video-16: [Revoir comment démontrer qu'un triangle est (ou n'est pas) rectangle](https://www.youtube.com/watch?v=hLf2IlEUcro){:target="_blank"}  
    ![](../images/logo_rapematiques.png) [Rapématiques](https://www.youtube.com/watch?v=F-jkF9xars4){:target="_blank"}  
    ![](../images/logo_hachette.png) [S'entraîner à déterminer si un triangle est rectangle](https://college.hachette-education.com/ressources/0002017158318/cami3_C11_002_103_exointer/){:target="_blank"}  

    ### **4) Calculer la longueur d'un côté dans un triangle rectangle**
    > :four: Quand on connaît la longueur de deux côtés sur trois d’un triangle rectangle, comment calculer la longueur du 3e côté ?  
    > :five: Comment déterminer l’arrondi au dixième d’un nombre ?  

    :octicons-video-16: [Revoir comment utiliser le théorème de Pythagore](https://www.youtube.com/watch?v=XjLUW9Jl5UY){:target="_blank"}  
    ![](../images/logo_rapematiques.png) [Rapématiques](https://www.youtube.com/watch?v=Jv__hIb6Zi4){:target="_blank"}  
    ![](../images/logo_hachette.png) [S'entraîner à utiliser l'égalité de Pythagore](https://college.hachette-education.com/ressources/0002017158318/cami3_c11_001_102_Exointer/){:target="_blank"}  
    ![](../images/logo_coopmaths.png) [S'entraîner à trouver la valeur arrondie d'une racine carrée](https://coopmaths.fr/alea/?uuid=9c484&id=4G20-5&n=3&d=10&s=1&s2=true&cd=1&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  



??? note "03 La notion de fonction"
    :construction:

??? note "04 Les puissances"
    :construction:

??? note "05 Les statistiques"
    :construction:

??? note "06 Le calcul littéral"
    :construction:

??? note "07 Les transformations du plan"
    :construction:

??? note "08 La proportionnalité"
    :construction:

??? note "09 L'arithmétique"
    :construction:

??? note "10 Les probabilités"
    :construction:

??? note "11 Le théorème de Thalès"
    :construction:

??? note "12 Les équations"
    :construction:

??? note "13 La trigonométrie"
    :construction:

??? note "14 La géométrie dans l'espace"
    :construction:

??? note "15 La notion de ratio"
    :construction:

??? note "16 Des fonctions particulières : affines, linéaires, constantes"
    :construction:

??? note "17 Les grandeurs composées"
    :construction: