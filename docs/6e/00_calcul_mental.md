---
author: ET
title: 🥋 Ceintures CM 6e
hide:
  - footer
---

!!! example ""
    === ":fontawesome-regular-file-pdf:"
        |Le corrigé des fiches|Les fiches d'entraînement|
        |:-:|:-:|
        |[PDF](https://nuage03.apps.education.fr/index.php/s/5GrajFiQptJqkcG){:target="_blank"}|[PDF](https://nuage03.apps.education.fr/index.php/s/b5ozkHXzgRnEH5C){:target="_blank"}|


    === ":white_large_square: Blanche" 
        ??? question "1) Le critère de divisibilité par 2"
            Pour savoir si un nombre est divisible par 2, on regarde s'il finit par 0, 2, 4, 6 ou 8.  

            ^^Exemple 1^^ : 146 est divisible par 2 car il finit par 6.  

            ^^Exemple 2^^ : 2 307 n'est pas divisible par 2.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 12 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5ND2~o=0~q=0.~p=~t=12~n=6){:target="_blank"}   
        
        ??? question "2) La table de 2"
            Pour multiplier par 2, on calcule le double du nombre (on en prend deux fois plus).  

            ^^Exemple^^ : 7 × 2 = 14  
            
            !!! quote ""
                :material-weight-lifter: Maths Mentales : [Tables classiques - 6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=0~q=0.0,1~p=~t=15~n=6){:target="_blank"}  

                :material-weight-lifter:  CoopMaths : [Tables à trou - 6 questions](https://coopmaths.fr/alea/?uuid=0e6bd&id=6C10-1&n=6&d=10&s=2&s2=2&s3=true&i=1&cd=1&v=eleve&es=021100&title=){:target="_blank"}
    
        ??? question "3) Complément à 10"
            1 + 9 = 10  
            2 + 8 = 10  
            3 + 7 = 10  
            4 + 6 = 10  
            5 + 5 = 10  

            !!! quote ""
                :material-weight-lifter:  Maths Mentales : [6 questions - 12 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND3~o=0~q=0.0,1,2~p=~t=12~n=6){:target="_blank"}

        ??? question "4) La table de 5"
            Pour multiplier par 5, on peut multiplier par 10, puis prendre la moitié.  

            ^^Exemple^^ : 5 × 46 = 230  
            En effet, on calcule 46 × 10 = 460 puis 460 ÷ 2 = 230.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=3~q=3.0,1~p=~t=15~n=6){:target="_blank"}

        ??? question "5) La table de 10"
            Pour multiplier par 10 un nombre entier, on ajoute un zéro à la fin du nombre.  

            ^^Exemple^^ : 51 × 10 = 51**0**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=8~q=8.0,1~p=~t=15~n=6){:target="_blank"}

        ??? question "6) Ajouter 9 ; 19 ; 29"
            On ajoute d’abord la dizaine supérieure (10, 20 ou 30) puis on enlève 1 unité.  

            ^^Exemple 1^^ : 37 + 9 = 46 car 37 + 10 = 47 et 47 - 1 = 46  

            ^^Exemple 2^^ : 54 + 29 = 83 car 54 + 30 = 84 et 84 - 1 = 83  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8ND1~o=1~q=1.0,1~p=~t=18~n=6){:target="_blank"}
     
    === ":yellow_square: Jaune"
        ??? question "1) Le critère de divisibilité par 10"
            Pour savoir si un nombre est divisible par 10, on regarde s'il finit par 0.  

            ^^Exemple^^ : 210 est divisible par 10 car il finit par 0.  

            :material-weight-lifter: Maths Mentales : [6 questions - 12 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5ND2~o=4~q=2.-4.~p=~t=12~n=6){:target="_blank"}
    
        ??? question "2) La table de 3"
            Pour multiplier par 3, on calcule le triple du nombre (on en prend trois fois plus).  

            ^^Exemple^^ : 7 × 3 = 21  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=8,1~q=1.0,1-8.0,1~p=~t=15~n=6){:target="_blank"}

        ??? question "3) Double ou triple d'un entier"
            Pour prendre le double d'un nombre entier, on le multiplie par 2.  

            ^^Exemple 1^^ : Le double de 7 est 14 car 7 × 2 = 14.  

            Pour prendre le triple d'un nombre entier, on le multiplie par 3.  

            ^^Exemple 2^^ : Le triple de 7 est 21 car 7 × 3 = 21.  

            !!! quote ""
                :material-weight-lifter: Genially : [5 questions](https://view.genial.ly/60f5a4e20ddd920db2367d66){:target="_blank"}

        ??? question "4) Le critère de divisibilité par 5"
            Pour savoir si un nombre est divisible par 5, on regarde s'il finit par 0 ou 5.  

            ^^Exemple 1^^ : 215 est divisible par 5 car il finit par 5.  
                
            ^^Exemple 2^^ : 137 n'est pas divisible par 5.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 12 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5ND2~o=2~q=2.~p=~t=12~n=6){:target="_blank"}


        ??? question "5) La table de 4"
            Pour multiplier par 4 un nombre entier, on peut le multiplier par 2, puis encore par 2.  

            ^^Exemple^^ : 7 × 4 = 28 car 7 × 2 = 14 et 14 × 2 = 28  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=8,2~q=2.0,1-8.0,1~p=~t=15~n=6){:target="_blank"}


        ??? question "6) Soustraire 9 ; 19 ; 29"
            On enlève d’abord la dizaine supérieure (10, 20 ou 30) puis on ajoute 1 unité.  

            ^^Exemple^^ : 311 - 29 = 282 car 311 - 30 = 281 et 281 + 1 = 282  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8ND1~o=7~q=7.~p=~t=18~n=6){:target="_blank"}
 
    === ":orange_square: Orange"
        ??? question "1) La table de 11"
            11 × 1 = 11  
            11 × 2 = 22  
            11 × 3 = 33  
            11 × 4 = 44  
            11 × 5 = 55  
            11 × 6 = 66  
            11 × 7 = 77  
            11 × 8 = 88  
            11 × 9 = 99  
            11 × 10 = 110  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=9~q=9.0,1~p=~t=15~n=6){:target="_blank"}

        ??? question "2) Multiplier un entier par 10 ; 100 ; 1 000"
            Pour multiplier par 10 un nombre entier, on ajoute un zéro à la fin du nombre.  
            Pour multiplier par 100 un nombre entier, on ajoute deux zéros à la fin du nombre.  
            Pour multiplier par 1 000 un nombre entier, on ajoute trois zéros à la fin du nombre.  

            ^^Exemple^^ : 51 × 100 = 5 1**00**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=9NE1~o=0,1,2~q=0.0,1-1.0,1-2.0,1~p=~t=15~n=6){:target="_blank"}

        ??? question "3) Multiplier un nombre par 4"
            Pour multiplier par 4 un nombre entier, on peut le multiplier par 2, puis encore par 2.  

            ^^Exemple^^ : 28 × 4 = 112 car 28 × 2 = 56 et 56 × 2 = 112  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 16 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND17~o=4~q=4.0,1~p=~t=16~n=6){:target="_blank"}  

                :octicons-video-16: [Multiplier par 4](https://www.youtube.com/watch?v=sgCPBw9vvsM){:target="_blank"}

        ??? question "4) Les carrés des entiers de 1 à 10"
            Le carré d'un nombre est le produit du nombre par lui-même.  
            1 × 1 = 1  
            2 × 2 = 4  
            3 × 3 = 9  
            4 × 4 = 16  
            5 × 5 = 25  
            6 × 6 = 36  
            7 × 7 = 49  
            8 × 8 = 64  
            9 × 9 = 81  
            10 × 10 = 100  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND13~o=0~q=0.0,1~p=~t=15~n=6){:target="_blank"}

        ??? question "5) Additionner des nombres entiers"
            On veille à ne pas oublier la **retenue**.  

            ^^Exemple^^ : 45 + 18 = 63 car 5 + 8 = 13 (le chiffre des unités sera 3 et on retient **1**) et 4 + 1 + **1** = 6 (chiffre des dizaines).  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND1~o=2~q=2.~p=~t=15~n=6){:target="_blank"}

        ??? question "6) Multiplier un nombre par 20"
            Pour multiplier par 20, on multiple d'abord par 2, puis on multiplie par 10.  

            ^^Exemple^^ : 16 × 20 = 320 car 16 × 2 = 32 et puis 32 × 10 = 320

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 16 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND17~o=2~q=2.0,1~p=~t=16~n=6){:target="_blank"}  

                :octicons-video-16: [Multiplier par 20 ou par 50](https://www.youtube.com/watch?v=LHRQWpovfKA){:target="_blank"}

    === ":green_square: Verte"
        ??? question "1) La table de 7"
            7 × 1 = 7  
            7 × 2 = 14  
            7 × 3 = 21  
            7 × 4 = 28  
            7 × 5 = 35  
            7 × 6 = 42  
            7 × 7 = 49  
            7 × 8 = 56  
            7 × 9 = 63  
            7 × 10 = 70  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=5~q=5.0,1~p=~t=15~n=6){:target="_blank"}
    
        ??? question "2) La table de 6"
            6 × 1 = 6  
            6 × 2 = 12  
            6 × 3 = 18  
            6 × 4 = 24  
            6 × 5 = 30  
            6 × 6 = 36  
            6 × 7 = 42  
            6 × 8 = 48  
            6 × 9 = 54  
            6 × 10 = 60  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=4~q=4.0,1~p=~t=15~n=6){:target="_blank"}
    
        ??? question "3) Multiplier ou diviser par 10 ; 100 ; 1 000"
            Dans la multiplication par 10, 100 ou 1 000, le nombre devient plus grand : chaque chiffre gagne 1, 2 ou 3 rangs dans l’écriture décimale. Le **chiffre** des unités est le plus facile à repérer.  

            ^^Exemple 1^^ : 4**2**,6 × 100 = 4 **2**60  
            Le chiffre des unités, le **2**, devient le chiffre des centaines (multiplication par 100).  
 
            Dans la division par 10, 100 ou 1000, le nombre devient plus petit : chaque chiffre perd 1, 2 ou 3 rangs dans l’écriture décimale.  

            ^^Exemple 2^^ : 7 41**3** ÷ 100 = 74,1**3**  
            Le chiffre des unités, le **3**, devient le chiffre des centièmes (division par 100).  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [multiplier - 6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8NE4~o=0,1,2,3,4,5,6,7,8~q=0.-1.-2.-3.-4.-5.-6.-7.-8.~p=~t=18~n=6){:target="_blank"}  

                :material-weight-lifter: Maths Mentales : [diviser - 6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8NE5~o=0,1,2,3,4,5,6,7,8~q=0.-1.-2.-3.-4.-5.-6.-7.-8.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Multiplier par 10, 100 ou 1 000](https://www.youtube.com/watch?v=pPnCPmWGqyo){:target="_blank"}  

                :octicons-video-16: [Diviser par 10, 100 ou 1 000](https://www.youtube.com/watch?v=pLlBl2V1CC4){:target="_blank"}
    
        ??? question "4) Les critères de divisibilité par 3 et par 9"
            Pour savoir si un nombre est divisible par 3, on additionne tous ses chiffres et on regarde si le résultat est dans la table de 3.  

            ^^Exemple 1^^ : 525 est-il divisible par 3 ?  
            5 + 2 + 5 = 12 et 12 est dans la table de 3 donc oui, 525 est divisible par 3.  

            ^^Exemple 2^^ : 743 est-il divisible par 3 ?  
            7 + 4 + 3 = 14 et 14 n'est pas dans la table de 3 donc non, 743 n'est pas divisible par 3.  
 
            Pour savoir si un nombre est divisible par 9, on additionne tous ses chiffres et on regarde si le résultat est dans la table de 9.  

            ^^Exemple 3^^ : 684 est-il divisible par 9 ?  
            6 + 8 + 4 = 18 et 18 est dans la table de 9 donc oui, 684 est divisible par 9.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5ND2~o=1,3~q=1.-3.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Vérifier si un nombre est divisible par 3](https://www.youtube.com/watch?v=WVUh_b_uROk){:target="_blank"}  

                :octicons-video-16: [Vérifier si un nombre est divisible par 9](https://www.youtube.com/watch?v=Sz8HuHAZYHQ){:target="_blank"}
    
        ??? question "5) Soustraire ou additionner des décimaux"
            On veille à additionner ou soustraire rang par rang les chiffres de chaque nombre.  
            On peut **décomposer** l'un des 2 nombres pour calculer plus facilement.  

            ^^Exemple 1^^ : 7,2 + 8,6 = 15,8 (il n'y a pas de retenue dans ce calcul.)  

            ^^Exemple 2^^ : 4,7 + 6,9 = 11,6 car en décomposant **6,9** : 4,7 + **0,9** = 5,6 et 5,6 + **6** = 11,6  

            ^^Exemple 3^^ : 5,6 - 1,8 = 3,8 car en décomposant **1,8** : 5,6 - **0,8** = 4,8 et 4,8 - **1** = 3,8  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8NE1~o=0,1~q=0.-1.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer une addition ou une soustraction](https://www.youtube.com/watch?v=-KRBP9Ry0LA){:target="_blank"}
    
        ??? question "6) Multiplier un nombre par 50 ou 0,5"
            Pour multiplier un nombre par 50, on peut d'abord le multiplier par 100 puis prendre la moitié du résultat obtenu.  

            ^^Exemple 1^^ : 21 × 50 = 1 050 car 21 × 100 = 2 100 et puis 2 100 ÷ 2 = 1 050  
 
            Pour multiplier un nombre par 0,5, cela revient à le diviser par 2, donc à en prendre la moitié.  

            ^^Exemple 2^^ : 34 × 0,5 = 34 ÷ 2 = 17  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND17~o=3,6~q=3.0,1-6.0,1~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Multiplier par 0,5](https://www.youtube.com/watch?v=SgKpjbooXLE){:target="_blank"}
    
    === ":blue_square: Bleue"
        ??? question "1) La table de 8"
            8 × 1 = 8  
            8 × 2 = 16  
            8 × 3 = 24  
            8 × 4 = 32  
            8 × 5 = 40  
            8 × 6 = 48  
            8 × 7 = 56  
            8 × 8 = 64  
            8 × 9 = 72  
            8 × 10 = 80  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=6~q=6.0,1~p=~t=15~n=6){:target="_blank"}
    
        ??? question "2) Multiplier un nombre par 9 et par 99"
            Pour multiplier un nombre par 9, on peut le multiplier par 10, puis retirer une fois ce nombre.  

            ^^Exemple 1^^ : 9 × 68 = 612 car 10 × 68 = 680 et puis 680 - 68 = 612  

            Pour multiplier un nombre par 99, on peut le multiplier par 100, puis retirer une fois ce nombre.  

            ^^Exemple 2^^ : 99 × 7 = 693 car 100 × 7 = 700 et 700 - 7 = 693  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [par 9 - 6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND18~o=2~q=2.0,1~p=~t=18~n=6){:target="_blank"}  

                :material-weight-lifter: Maths Mentales : [par 99 - 6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND19~o=3~q=3.0,1~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Multiplier par 9](https://www.youtube.com/watch?v=bpmE9fW8sZQ){:target="_blank"}
    
        ??? question "3) La table de 9"
            9 × 1 = 9  
            9 × 2 = 18  
            9 × 3 = 27  
            9 × 4 = 36  
            9 × 5 = 45  
            9 × 6 = 54  
            9 × 7 = 63  
            9 × 8 = 72  
            9 × 9 = 81  
            9 × 10 = 90  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=7~q=7.0,1~p=~t=15~n=6){:target="_blank"}
    
        ??? question "4) Calculer la moitié, le tiers et le quart"
            Prendre la moitié d'un nombre revient à le diviser par 2.  
            Prendre le tiers d'un nombre revient à le diviser par 3.  
            Prendre le quart d'un nombre revient à le diviser par 4 (on divise par 2 deux fois de suite).  

            ^^Exemple 1^^ : 450 ÷ 3 = 150  

            ^^Exemple 2^^ : 36 ÷ 4 = 9 car 36 ÷ 2 = 18 et puis 18 ÷ 2 = 9  

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/60fecb405825d50ddf886ee0){:target="_blank"}  

                :octicons-video-16: [Diviser par 4](https://www.youtube.com/watch?v=S00DJpin0QI){:target="_blank"}
    
        ??? question "5) Écrire sous la forme d'une fraction décimale"
            ^^Exemple 1^^ : 9,**1** c’est 91 **dixièmes** donc on peut l’écrire $\dfrac{91}{10}$.    
 
            ^^Exemple 2^^ : 12,**64** c’est 1 264 **centièmes** donc on peut l’écrire $\dfrac{1 264}{100}$.  
 
            ^^Exemple 3^^ : 6,**053** c’est 6 053 **millièmes** donc on peut l’écrire $\dfrac{6 053}{1 000}$.  

            !!! quote ""
                :material-weight-lifter: Genially : [5 questions](https://view.genial.ly/60fecb405825d50ddf886ee0){:target="_blank"}  

                :octicons-video-16: [Passer de la fraction décimale à l'écriture décimale](https://www.youtube.com/watch?v=i75HKdds3Gc){:target="_blank"}
    
        ??? question "6) Multiplier un nombre par 0,1 ; 0,01 ; 0,001"
            Multiplier un nombre par 0,1 revient à le diviser par 10.  
            Multiplier un nombre par 0,01 revient à le diviser par 100.  
            Multiplier un nombre par 0,001 revient à le diviser par 1 000.  
            Le nombre devient donc plus petit : chaque chiffre du nombre perd 1, 2 ou 3 rang dans l’écriture décimale.  

            ^^Exemple^^ : 2**3**,26 × 0,1 = 2**3**,26 ÷ 10 = 2,**3**26  
            Le chiffre des unités, le **3**, devient le chiffre des dixièmes (division par 10).  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6NE2~o=0,1,2~q=0.0,1-1.0,1-2.0,1~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Multiplier par 0.1, 0.01 ou 0.001](https://www.youtube.com/watch?v=yKXry2gyoa8){:target="_blank"}
    
    === ":brown_square: Marron"
        ??? question "1) Toutes les tables"
            !!! quote ""            
                :material-weight-lifter: Maths Mentales : [10 questions - 12 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=0,1,2,3,4,5,6,7,8~q=0.0,1-1.0,1-2.0,1-3.0,1-4.0,1-5.0,1-6.0,1-7.0,1-8.0,1~p=~t=12~n=10){:target="_blank"}  

                :material-weight-lifter: [36 questions pour un champion](https://www.mpoulain.fr/multiplication/){:target="_blank"}  
    
        ??? question "2) Sous la forme d’un entier et d’une fraction < 1"
            On souhaite **décomposer** un nombre en la somme d'un nombre entier et d'une fraction inférieure à 1.  

            ^^Exemple 1^^ : Décompose $\dfrac{11}{4}$.  
            On cherche le nombre le plus proche de 11, dans la table de 4, mais sans le dépasser : c'est 8 (4 × 2).  
            $\dfrac{11}{4} = \dfrac{8}{4} + \dfrac{3}{4} = 2 + \dfrac{3}{4}$  
 
            ^^Exemple 2^^ : Décompose $\dfrac{46}{7}$.
            On cherche le nombre le plus proche de 46, dans la table de 7, mais sans le dépasser : c'est 42 (7 × 6).  
            $\dfrac{46}{7} = \dfrac{42}{7} + \dfrac{4}{7} = 6 + \dfrac{4}{7}$  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6NB4~o=0~q=0.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Ecrire une fraction comme la somme d'un entier et d'une fraction inférieure à 1](https://www.youtube.com/watch?v=NZIGqHLoWFE){:target="_blank"}
    
        ??? question "3) Additionner des nombres astucieusement"
            On peut changer l'ordre des termes et les **regrouper** de manière astucieuse (pour obtenir un nombre entier, facile à calculer).  

            ^^Exemple^^ : 1,4 + 18 + 2,6 = 22 car 1,4 + 2,6 = 4 et puis 4 + 18 = 22  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=9NE3~o=4~q=4.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Calculer astucieusement](https://www.youtube.com/watch?v=jqrdOtWXxkU){:target="_blank"}
    
        ??? question "4) Additions et soustractions de fractions"
            On est dans le cas où les dénominateurs sont les mêmes.  
            Pour trouver le résultat, on ajoute les numérateurs et on garde le dénominateur commun.  

            ^^Exemple 1^^ : $\dfrac{5}{3} + \dfrac{8}{3} = \dfrac{5+8}{3} = \dfrac{13}{3}$  

            ^^Exemple 2^^ : $\dfrac{85}{100} - \dfrac{6}{100} = \dfrac{85-6}{100} = \dfrac{79}{100}$  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC7~o=1~q=0.-1.~p=~t=15~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer une addition ou une soustraction de fractions de même dénominateur](https://www.youtube.com/watch?v=2-JfYiX6Wk4){:target="_blank"}
    
        ??? question "5) Prendre une fraction d’une quantité"
            ^^Exemple 1^^ : calcule les $\dfrac{2}{3}$ de 30 €.  
            On prend d'abord le tiers de 30 €, puis on le multiplie par 2.  
            30 ÷ 3 = 10, puis 10 × 2 = **20 €**  

            ^^Exemple 2^^ : calcule les $\dfrac{4}{7}$ de 42 L.  
            On divise d'abord 42 L par 7, puis on le multiplie par 4.  
            42 ÷ 7 = 6, puis 6 × 4 = **24 L**  

            ^^Exemple 3^^ : calcule les trois quarts de 120 cm.  
            On divise 120 cm par 4, puis on le multiplie par 3.  
            120 ÷ 4 = 30, puis 30 × 3 = **90 cm**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=7NB1~o=0,1~q=0.0,1-1.0,1~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Calculer la fraction d'une quantité](https://www.youtube.com/watch?v=ifOuBXR1Ums){:target="_blank"}
    
        ??? question "6) Appliquer un pourcentage"
            50 % d'un nombre, c'est prendre la moitié.  
            25 % d'un nombre, c'est prendre le quart.  
            75 % d'un nombre, c'est prendre les trois quarts.  
            10 % d'un nombre, c'est prendre le dixième.  
            100 % d'un nombre, c'est prendre la totalité.  

            ^^Exemple 1^^ : 10 % de 60 € ?  
            60 ÷ 10 = **6 €**  

            ^^Exemple 2^^ : 25 % de 60 € ?  
            60 ÷ 4 = **15 €**  

            ^^Exemple 3^^ : 75 % de 60 € ?  
            60 ÷ 4 = 15, puis 15 × 3 = **45 €**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC10~o=0,2,4,5~q=0.0,1-2.0,1-4.0,1-5.0,1~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Appliquer un pourcentage](https://www.youtube.com/watch?v=Ce6E56gsbY0){:target="_blank"}
    
    === ":black_large_square: Noire"
        ??? question "1) Calculer avec la distributivité"
            ^^Exemple 1^^ : 12 × 8 + 12 × 2 ?  
            On remarque qu'il y a 8 lots de 12 et 2 lots de 12.  
            Donc on aura 10 lots de 12.  
            12 × 10 = **120**  
            
            ^^Exemple 2^^ : 8 × 12 ?  
            On peut, par exemple, décomposer 12 en 10 + 2.  
            Donc cela revient à calculer 8 × 10 = 80 et 8 × 2 = 16.  
            Puis 80 + 16 = **96**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND22~o=3~q=3.0,1~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Appliquer la distributivité pour le calcul mental 1](https://www.youtube.com/watch?v=qKjt5bPMB5M){:target="_blank"}  

                :octicons-video-16: [Appliquer la distributivité pour le calcul mental 2](https://www.youtube.com/watch?v=B16mT1yTF8I){:target="_blank"}
   
        ??? question "2) Multiplier par 25 ; 1,5 ; 2,5"
            Pour multiplier un nombre par 25, on peut d'abord le multiplier par 100 puis en prendre le quart.  
            ^^Exemple 1^^ : 21 × 25 ?  
            21 × 100 = 2 100, puis 2 100 ÷ 4 = **525**  
 
            Pour multiplier un nombre par 1,5, on le prend une fois puis on rajoute la moitié de ce nombre.  
            ^^Exemple 2^^ : 34 × 1,5 ?  
            34   
            34 ÷ 2 = 17  
            Donc 34 + 17 = **51**  
 
            Pour multiplier un nombre par 2,5, on peut le double du nombre puis on rajoute la moitié de ce nombre.  
            ^^Exemple 3^^ : 12 × 2,5 ?  
            12 × 2 = 24  
            12 ÷ 2 = 6  
            Donc 24 + 6 = **30**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [par 25 - 6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND17~o=5~q=5.0,1~p=~t=18~n=6){:target="_blank"}  

                :material-weight-lifter: Maths Mentales : [par 1,5 - 6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6NE3~o=0,1~q=0.0,1-1.0,1~p=~t=18~n=6){:target="_blank"}  

                :material-weight-lifter: Maths Mentales : [par 2,5 - 6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6NE3~o=2,3~q=2.0,1-3.0,1~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Multiplier par 25](https://www.youtube.com/watch?v=Jn5-5FaLlXg){:target="_blank"}     
   
        ??? question "3) Calculer avec les priorités opératoires"
            Dans un calcul sans parenthèses et formé uniquement d’additions et de soustractions, les calculs s’effectuent de gauche à droite.  

            ^^Exemple 1^^ : **64 – 9** + 1 = **55** + 1 = 56  
 
            Dans un calcul sans parenthèses, la multiplication et la division sont effectuées en priorité sur l’addition et la soustraction.  

            ^^Exemple 2^^ : 5 + **3 × 9** = 5 + **27** = 32  
 
            Dans un calcul avec parenthèses, les calculs entre parenthèses sont effectués en priorité.  
            ^^Exemple 3^^ : (**6 + 7**) × (**14 - 4**) = **13** × **10** = 130  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND20~o=0~q=0.0,1,2,3~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer des calculs avec des priorités](https://www.youtube.com/watch?v=a-IG_bjKeJc){:target="_blank"}
   
        ??? question "4) Reste d’une division euclidienne"
            Le quotient est le facteur le plus grand qui donne un produit inférieur au dividende (le nombre de départ), et le reste est ce qu’il manque pour y arriver.  

            ^^Exemple^^ : 114 ÷ 15 ?  
            Le quotient est 7 et le reste est 9 car 15 × **7** = 105 et 105 + **9** = 114  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND10~o=2~q=2.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Trouver le quotient et le reste d'une division simple](https://www.youtube.com/watch?v=9UoySvJ1ukU){:target="_blank"}
   
        ??? question "5) Multiplier astucieusement"
            Il est utile de connaître les multiplications suivantes :  
            0,1 × 10 = 1  
            0,01 × 100 = 1  
            5 × 2 = 10  
            5 × 0,2 = 1  
            0,5 × 2 = 1  
            4 × 25 = 100  
            4 × 2,5 = 10  
            4 × 0,25 = 1  
            8 × 1,25 = 10  

            De plus, on peut changer l'ordre des facteurs.  

            ^^Exemple^^ : 25 × 8 × 7 × 4 ?  
            Comme 25 × 4 = 100 et 8 × 7 = 56, on a donc : 25 × 8 × 7 × 4 = 5 600  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6NE6~o=0,1,3~q=0.0,1,2,3-1.0,1,2,3-3.0,1,2,3~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Calculer astucieusement](https://www.youtube.com/watch?v=jqrdOtWXxkU){:target="_blank"}
   
        ??? question "6) Conversions"
            Pour convertir des **durées**, il est important de savoir que :  
            - 60 min = 1 heure  
            - 30 min = ½ heure = 0,5 h  
            - 15 min = ¼ heure = 0,25 h  
            - 45 min = ¾ heure = 0,75 h  

            ^^Exemple 1^^ : 1,5 h = 1 h + 0,5 h = 60 min + 30 min = 90 min  

            Pour convertir des unités de **longueur** : km - hm - dam - m - dm - cm - mm  
            Pour convertir des unités de **masse** : kg - hg - dag - g - dg - cg - mg  
            1 tonne = 1 000 kg  
            Pour convertir des unités de **capacité** : kL - hL - daL - L - dL - cL - mL  

            ^^Exemple 2^^ : 3,4 m = 340 cm  

            ^^Exemple 3^^ : 9,8 cm = 0,0098 dam   

            ^^Exemple 4^^ : 0,5 L = 5 dL = 50 cL  

            Il est important d’être à l’aise avec 2 choses : le tableau des unités et savoir trouver le chiffre des unités d’un nombre décimal.  
            La conversion se fait en 2 étapes :  
            - On place le nombre donné en mettant le chiffre des unités dans la colonne de l’unité fournie.  
            - On écrit le nombre décimal en prenant comme nouveau chiffre des unités celui de la nouvelle unité (on "déplace la virgule"). On devra parfois combler avec des zéros.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8MA1~o=0~q=0.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Convertir les unités de longueur](https://www.youtube.com/watch?v=a6rFbX2eRx4){:target="_blank"}   



<!-- Objectifs du livret CM -->

:dart: **Les objectifs des ceintures CM :**  
- Meilleure maîtrise des tables de multiplication  
- Meilleure maîtrise des méthodes simples de calcul mental  
- Travailler en temps limité  
- Développer des automatismes  
- Apprendre la logique  
- Apprendre à raisonner  
- Apprendre à travailler en autonomie  

:material-weight-lifter: Les **efforts** que tu fourniras tout au long de l'année pour réussir chacune de ces ceintures t'amèneront à avoir plus de connaissances et plus de facilités à travailler.  

![](../images/logo_cbpm.png){ width=5% }