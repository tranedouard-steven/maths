---
author: ET
title: 💻 Numérique 6e
hide:
  - footer
---

!!! note "Apprendre à écrire rapidement sur un clavier"
    :globe_with_meridians: [S'entraîner avec Edclub](https://annot.edclub.com/){:target="_blank"}  

!!! note "Algorithmique et programmation 1"
    :globe_with_meridians: [Le chevalier de la programmation](https://www.logicieleducatif.fr/games/le-chevalier-de-la-programmation3/index.html){:target="_blank"}  

!!! note "Algorithmique et programmation 2"
    :globe_with_meridians: [Coder un jeu avec Scratch avec 18 vidéos tutos](https://www.cahiernum.net/){:target="_blank"}  