---
author: ET
title: 📓 Leçons 6e
hide:
  - footer
---

??? note "01 Les nombres entiers"
    !!! danger ""
        === "Je révise"
            📄 Fiche de mémorisation : :construction:  
            🗃️ Carte mentale : [Myriade](https://nuage03.apps.education.fr/index.php/s/tC2ANmga9jgB3km){:target="_blank"}  
            ☑️ QCM : :construction:        
            🎮 Genially : :construction:

        === "J'approfondis"
            :octicons-video-16: Petits contes mathématiques : [C'est quoi le zéro ?](https://www.lumni.fr/video/petits-contes-mathematiques-le-zero#containerType=program&containerSlug=petits-contes-mathematiques){:target="_blank"}  

    ### **1) Écrire et lire des nombres entiers**
    > :one: Quelles sont les 2 caractéristiques de la numération que nous utilisons ?  
    > :two: Comment lire facilement les très grands nombres ?  
    > :three: Comment écrire les nombres entiers en lettres ?  

    :octicons-video-16: [Revoir comment lire et écrire un nombre entier](https://college.hachette-education.com/ressources/0002017190998/01_video-methode.mp4){:target="_blank"}  
    ![](../images/logo_coopmaths.png) [S'entraîner à écrire un nombre entier en chiffres](https://coopmaths.fr/alea/?uuid=0688e&id=6N10&n=3&d=10&s=7-5-4&s2=2-5&s3=2&cd=1&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  
    ![](../images/logo_coopmaths.png) [S'entraîner à écrire un nombre entier en lettres](https://coopmaths.fr/alea/?uuid=0688e&id=6N10&n=2&d=10&s=7-5&s2=2&s3=1&cd=1&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  
    ![](../images/logo_coopmaths.png) [S'entraîner à écrire correctement les grands nombres entiers](https://coopmaths.fr/alea/?uuid=dc348&id=6N10-4&n=2&d=10&cd=1&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  
    ![](../images/logo_mathix.png) [S'entraîner à écrire un entier en lettres sur un chèque au clavier](https://www.mathix.org/nombre_en_lettres/index.html){:target="_blank"}  

    ### **2) Trouver le chifre des ... ou le nombre des ...**
    > :four: Comment trouver le chiffre des ... d'un nombre entier ?  comment trouver le nombre des ... d'un nombre entier ?"  

    ![](../images/logo_hachette.png) [S'entraîner à lire et écrire des nombres entiers](https://college.hachette-education.com/ressources/0002016291948/cami6_c01_001_004_exointer/){:target="_blank"}  
    ![](../images/logo_coopmaths.png) [S'entraîner à déterminer le chiffre des … et le nombre des …](https://coopmaths.fr/alea/?uuid=34579&id=6N10-3&n=4&d=10&s=3&s2=3&cd=1&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  
    ![](../images/logo_genially.png) [S'entraîner à distinguer : chiffre des..., nombre des...](https://view.genial.ly/5e9ee02bbc1ba60db2ff7865){:target="_blank"}  

    ### **3) Décomposer un nombre entier**
    > :five: Comment décomposer un nombre entier selon ses rangs ?  

    ![](../images/logo_coopmaths.png) [S'entraîner à décomposer un nombre entier](https://coopmaths.fr/alea/?uuid=f899b&id=6N10-7&n=2&d=10&s=5&s2=7&s3=5-7&s4=0&cd=1&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  
    ![](../images/logo_coopmaths.png) [S'entraîner à recomposer un nombre entier](https://coopmaths.fr/alea/?uuid=f899b&id=6N10-7&n=2&d=10&s=5&s2=6&s3=9&s4=0&cd=1&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  

    ### **4) Repérer des points sur une demi-droite graduée**
    > :six: Comment repérer des points sur une demi-droite graduée ?  

    ![](../images/logo_coopmaths.png) [S'entraîner à placer un point d'abscisse entière](https://coopmaths.fr/alea/?uuid=4f2a3&id=6N11-2&n=1&d=10&s=1&cd=1&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  
    ![](../images/logo_coopmaths.png) [S'entraîner à lire l'abscisse entière d'un point](https://coopmaths.fr/alea/?uuid=acd4a&id=6N11&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  

    ### **5) Comparer des nombres entiers**
    > :seven: Comment comparer deux nombres entiers ?  
    > :eight: Quelles sont les 2 façons de ranger une suite de nombres ?  
    > :nine: Comment encadrer un nombre entier ?  

    ![](../images/logo_coopmaths.png) [S'entraîner à comparer deux nombres entiers](https://coopmaths.fr/alea/?uuid=a7aa7&id=6N11-5&n=4&d=10&s=3&cd=1&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  
    ![](../images/logo_coopmaths.png) [S'entraîner à ranger dans l'ordre croissant ou décroissant](https://coopmaths.fr/alea/?uuid=3bba9&id=6N11-4&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  
    ![](../images/logo_coopmaths.png) [S'entraîner à encadrer deux entiers](https://coopmaths.fr/alea/?uuid=29b40&id=6N11-3&v=confeleve&v=eleve&title=&es=021100){:target="_blank"}  



??? note "02 Les premiers éléments de géométrie"
    :construction:

??? note "03 Des fractions aux nombres décimaux"
    :construction:

??? note "04 Les droites"
    :construction:

??? note "05 Les nombres décimaux 2"
    :construction:

??? note "06 Les polygones et le cercle"
    :construction:

??? note "07 L'addition et la soustraction"
    :construction: