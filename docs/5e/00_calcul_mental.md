---
author: ET
title: 🥋 Ceintures CM 5e
hide:
  - footer
---

!!! example ""
    === ":fontawesome-regular-file-pdf:"
        |Le corrigé des fiches|Les fiches d'entraînement|
        |:-:|:-:|
        |[PDF](https://nuage03.apps.education.fr/index.php/s/PaBQLJ4ALDoEHEy){:target="_blank"}|[PDF](https://nuage03.apps.education.fr/index.php/s/6QfPMAnHR8TCADa){:target="_blank"}|


    === ":white_large_square: Blanche" 
        ??? question "1) Critères de divisibilité par 2, 5, 10"
            Pour savoir si un nombre est divisible par 2, on regarde s'il finit par 0, 2, 4, 6 ou 8.  

            ^^Exemple 1^^ : 146 est divisible par 2 car il finit par 6.  
            ^^Exemple 2^^ : 2 307 n'est pas divisible par 2.  

            Pour savoir si un nombre est divisible par 5, on regarde s'il finit par 0 ou 5.  

            ^^Exemple 3^^ : 215 est divisible par 5 car il finit par 5.                  
            ^^Exemple 4^^ : 137 n'est pas divisible par 5.  

            Pour savoir si un nombre est divisible par 10, on regarde s'il finit par 0.  

            ^^Exemple 5^^ : 390 est divisible par 10 car il finit par 0.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 12 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5ND2~o=0,2,4~q=0.-2.-4.~p=~t=12~n=6){:target="_blank"}

        ??? question "2) Double, triple, quadruple"
            Pour prendre le double d'un nombre entier, on le multiplie par 2.  

            ^^Exemple 1^^ : Le double de 7 est 14 car 7 × 2 = 14.  
            
            Pour prendre le triple d'un nombre entier, on le multiplie par 3.  

            ^^Exemple 2^^ : Le triple de 7 est 21 car 7 × 3 = 21.  

             Pour prendre le quadruple d'un nombre entier, on le multiplie par 4 (c'est-à-dire par 2 et encore par 2).  

            ^^Exemple 4^^ : Le quadruple de 7 est 28 car 7 × 4 = 28.  

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/6359234bd79c10001842372c){:target="_blank"}

        ??? question "3) Tables de multiplication"
            !!! quote ""            
                :material-weight-lifter: Maths Mentales : [10 questions - 12 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND6~o=0,1,2,3,4,5,6,7,8~q=0.0,1-1.0,1-2.0,1-3.0,1-4.0,1-5.0,1-6.0,1-7.0,1-8.0,1~p=~t=12~n=10){:target="_blank"}  

                :material-weight-lifter: [36 questions pour un champion](https://www.mpoulain.fr/multiplication/){:target="_blank"}  

        ??? question "4) Division par 2"
            Pour diviser par 2, il suffit de prendre la moitié.  

            ^^Exemple^^ : 34 ÷ 2 = 17 car la moitié de 34 est 17.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6NE7~o=0~q=0.~p=~t=15~n=6){:target="_blank"}

        ??? question "5) Addition et soustraction de décimaux"
            On veille à additionner ou soustraire rang par rang les chiffres de chaque nombre.  
            On peut **décomposer** l'un des 2 nombres pour calculer plus facilement.  

            ^^Exemple 1^^ : 7,2 + 8,6 = 15,8 (il n'y a pas de retenue dans ce calcul.)  

            ^^Exemple 2^^ : 4,7 + 6,9 = 11,6 car en décomposant **6,9** : 4,7 + **0,9** = 5,6 et 5,6 + **6** = 11,6  

            ^^Exemple 3^^ : 5,6 - 1,8 = 3,8 car en décomposant **1,8** : 5,6 - **0,8** = 4,8 et 4,8 - **1** = 3,8  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8NE1~o=2,1~q=1.-2.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer une addition ou une soustraction](https://www.youtube.com/watch?v=-KRBP9Ry0LA){:target="_blank"}

        ??? question "6) Calcul astucieux avec 99 ; 101 ; …"
            Pour multiplier par 99, on le multiplie par 100 et on soustrait ce nombre.  
            ^^Exemple 1^^ : 36 × 99 = ?  
            36 × 100 = 3 600  
            3 600 - 36 = **3 564**  

            ^^Exemple 2^^ : 50 × 99 = ?  
            55 × 100 = 5 000  
            5 000 - 50 = **4 950**  
 
            Pour multiplier par 101, on le multiplie par 100 et on ajoute ce nombre.  

            ^^Exemple 3^^ : 36 × 101 = ?  
            36 × 100 = 3 600  
            3 600 + 36 = **3 636**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND19~o=0,3~q=0.0,1-3.0,1~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Multiplier par 99](https://www.youtube.com/watch?v=Ik8ueBrNSWs){:target="_blank"}  

                :octicons-video-16: [Multiplier par 101](https://www.youtube.com/watch?v=JZqEnltf90A){:target="_blank"}
     
    === ":yellow_square: Jaune"
        ??? question "1) Critères de divisibilité"
            Un nombre entier est divisible  
            - par 2 : s’il est pair (il finit par 0, 2, 4, 6 ou 8).  
            - par 5 : s’il finit par 5 ou 0.  
            - par 10 : s’il finit par 0.  
            - par 3 (ou 9) : si la somme des chiffres du nombre est divisible par 3 (ou 9).  

            ^^Exemple 1^^ : 345 est-il divisible par 9 ?  
            3 + 4 + 5 = 12.  
            12 n’est pas dans la table de 9 donc 345 n’est pas divisible par 9 (mais par 3, oui car 12 est dans la table des 3).  

            ^^Exemple 2^^ : 46 est-il divisible par 3 ?  
            4 + 6 = 10.  
            10 n’est pas dans la table des 3 donc 46 n’est pas divisible par 3.  

            !!! quote ""
                material-weight-lifter: Maths Mentales : [6 questions - 14 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5ND2~o=0,1,2,3,4~q=0.-1.-2.-3.-4.~p=~t=14~n=6){:target="_blank"}  

                :octicons-video-16: [Appliquer les critères de divisibilité](https://www.youtube.com/watch?v=BJDE6uOrmYQ){:target="_blank"}

        ??? question "2) Moitié, tiers, quart"
            Prendre la moitié d'un nombre revient à le diviser par 2.  
            Prendre le tiers d'un nombre revient à le diviser par 3.  
            Prendre le quart d'un nombre revient à le diviser par 4 (on divise par 2 deux fois de suite).  

            ^^Exemple 1^^ : 450 ÷ 3 = 150  

            ^^Exemple 2^^ : 36 ÷ 4 = 9 car 36 ÷ 2 = 18 et puis 18 ÷ 2 = 9  

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/5fdb0d9e3c971e0d6598e81f){:target="_blank"}

        ??? question "3) Calculer astucieusement avec des décimaux"
            On peut changer l'ordre des termes et les **regrouper** de manière astucieuse (pour obtenir un nombre entier, facile à calculer).  

            ^^Exemple^^ : 1,4 + 18 + 2,6 = 22 car 1,4 + 2,6 = 4 et puis 4 + 18 = 22  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=7NE1~o=1,2~q=1.0,1,2-2.0,1,2~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Calculer astucieusement](https://www.youtube.com/watch?v=jqrdOtWXxkU){:target="_blank"}

        ??? question "4) Priorités de calcul Niveau 1"
            Dans un calcul sans parenthèses et formé uniquement d’additions et de soustractions, les calculs s’effectuent de gauche à droite.  

            ^^Exemple 1^^ : **64 – 9** + 1 = **55** + 1 = 56  
 
            Dans un calcul sans parenthèses, la multiplication et la division sont effectuées en priorité sur l’addition et la soustraction.  

            ^^Exemple 2^^ : 5 + **3 × 9** = 5 + **27** = 32  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 16 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND20~o=4,5~q=4.0,1,2,3-5.0,1,2,3~p=~t=16~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer des calculs](https://www.youtube.com/watch?v=TJH-fiwAt5s){:target="_blank"}


        ??? question "5) Conversion de durées Niveau 1"
            1 h = 60 min  
            1 min = 60 s  
            1 h = 3 600 s  

            ^^Exemple 1^^ : 150 min = 60 min + 60 min + 30 min = 2 h 30  

            ^^Exemple 2^^ : 135 s = 60 s + 60 s + 15 s = 2 min 15 s  

            ^^Exemple 3^^ : 4 h = 4 × 60 min = 240 min  
            
            ^^Exemple 4^^ : 3 min 18 = 3 × 60 s + 18 s = 180 s + 18 s = 198 s  

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/5fdb0bb8ebe82c0d17f18f9f){:target="_blank"}

        ??? question "6) Multiplication avec des décimaux"
            Pour multiplier avec des nombres décimaux, on les multiplie sans s’occuper de la virgule.  
            On compte ensuite le nombre total de chiffres après la virgule des nombres et on place la virgule au bon endroit dans le résultat.  

            ^^Exemple 1^^ : 4,5 × 3 ?  
            On calcule 45 × 3 = 135.  
            1 chiffre après la virgule dans 4,5 × 3 donc 4,5 × 3 = **13,5**  

            ^^Exemple 2^^ : 5,1 × 0,4 ?  
            On calcule 51 × 4 = 204.  
            2 chiffres après la virgule dans 5,1 × 0,4 donc 5,1 × 0,4 = **2,04**  

            !!! quote ""
                :material-weight-lifter: JPPJM : [10 questions](https://www.jepeuxpasjaimaths.fr/?ex=multiplicationdecimauxcycle3){:target="_blank"}
 
    === ":orange_square: Orange"
        ??? question "1) Diviser par 4"
            Diviser par 4 revient à diviser par 2 puis encore par 2.  

            ^^Exemple^^ : 36 ÷ 4 = 9  
            car 36 ÷ 2 = 18 et puis 18 ÷ 2 = 9  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6NE7~o=4~q=4.~p=~t=15~n=6){:target="_blank"}   

        ??? question "2) Priorités de calcul Niveau 2"
            Dans un calcul avec parenthèses, les calculs entre parenthèses sont effectués en priorité.  

            ^^Exemple 1^^ : 5 × (**6 + 2**) = 5 × **8** = 40

            ^^Exemple 2^^ : (**6 + 7**) × (**14 - 4**) = **13** × **10** = 130  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND20~o=3~q=3.0,1,2,3,4,5~p=~t=18~n=6){:target="_blank"}  

            :octicons-video-16: [Effectuer des calculs avec des priorités](https://www.youtube.com/watch?v=mLlLNM5D66M){:target="_blank"}

        ??? question "3) Multiple ou diviseur ?"
            Un multiple est un nombre que l’on obtient par multiplication.  

            ^^Exemple 1^^ : Des multiples de 4 sont 8, 12, 16, 20, 44, etc.  
 
            Un diviseur est un nombre qui divise un nombre tel que le résultat de la division soit un nombre entier.  

            ^^Exemple 2^^ : Les diviseurs de 12 sont 1, 2, 3, 4, 6 ou 12.  
            12 ÷ 1 = 12  
            12 ÷ 2 = 6  
            12 ÷ 3 = 4  
 
            Un nombre a une infinité de multiples, mais a un nombre fini des diviseurs (il y a toujours 1 et le nombre en question comme diviseur).  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6ND14~o=0,1~q=0.-1.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Reconnaître un multiple ou un diviseur]https://www.youtube.com/watch?v=-PLZFlAG99Q){:target="_blank"}

        ??? question "4) Conversion unités masse et longueur"
            Pour convertir des unités de **longueur** : km - hm - dam - m - dm - cm - mm  
            Pour convertir des unités de **masse** : kg - hg - dag - g - dg - cg - mg  
            1 tonne = 1 000 kg  

            ^^Exemple 1^^ : 3,4 m = 340 cm  

            ^^Exemple 2^^ : 9,8 cm = 0,0098 dam   

            ^^Exemple 3^^ : 0,5 g = 5 dg = 50 cg  

            Il est important d’être à l’aise avec 2 choses : le tableau des unités et savoir trouver le chiffre des unités d’un nombre décimal.  
            La conversion se fait en 2 étapes :  
            - On place le nombre donné en mettant le chiffre des unités dans la colonne de l’unité fournie.  
            - On écrit le nombre décimal en prenant comme nouveau chiffre des unités celui de la nouvelle unité (on "déplace la virgule"). On devra parfois combler avec des zéros.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=7MA1~o=0~q=0.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Convertir les unités de longueur](https://www.youtube.com/watch?v=a6rFbX2eRx4){:target="_blank"}

        ??? question "5) Périmètre de figures usuelles"
            Le périmètre d’une figure est la longueur de son contour.  

            - Formule du périmètre du carré : 4 × côté = 4 × c  
            - Formule du périmètre du rectangle : 2 × Longueur + 2 × largeur = 2 × L + 2 × l  
            - Formule du périmètre du cercle : 2 × π × rayon = 2 × π × r ou π × diamètre = π × d  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6MA1~o=5,4,0,1~q=0.-1.-4.-5.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Calculer le périmètre d'une figure](https://www.youtube.com/watch?v=w7n638xdT6E){:target="_blank"}  

                :octicons-video-16: [Calculer la longueur d'un cercle](https://www.youtube.com/watch?v=iKyAfCzKnu4){:target="_blank"}

        ??? question "6) Multiplication par 10 ; 100 ; 1000"
            Dans la multiplication par 10, 100 ou 1 000, le nombre devient plus grand : chaque chiffre gagne 1, 2 ou 3 rangs dans l’écriture décimale. Le **chiffre** des unités est le plus facile à repérer.  

            ^^Exemple^^ : 4**2**,6 × 100 = 4 **2**60  
            Le chiffre des unités, le **2**, devient le chiffre des centaines (multiplication par 100).  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 14 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8NE4~o=0,1,2,3,4,5,6,7,8~q=0.-1.-2.-3.-4.-5.-6.-7.-8.~p=~t=14~n=6){:target="_blank"}  

                :octicons-video-16: [Multiplier par 10, 100 ou 1 000](https://www.youtube.com/watch?v=pPnCPmWGqyo){:target="_blank"}

    === ":green_square: Verte"
        ??? question "1) Simplifier une expression littérale"
            Pour simplifier une expression littérale, on peut **enlever le symbole multiplié "×"** entre un nombre et une lettre.  
            
            ^^Exemple 1^^ : 3 × 𝑥 = 3𝑥  
 
            𝑎 × 𝑎 = 𝑎² (𝑎 au carré) et 𝑎 × 𝑎 × 𝑎 = 𝑎³ (𝑎 au cube)  
 
            On ne peut pas additionner des nombres et des lettres.  

            ^^Exemple 2^^ : 3 + 𝑛 reste 3 + 𝑛.  
 
            On peut écrire 𝑛 au lieu d’écrire 1𝑛.  
 
            Quand on a une multiplication mêlant nombre et lettre, on peut multiplier les nombres entre-eux et garder les lettres.  

            ^^Exemple 3^^ : 4 × 𝑥 × 2 × 𝑦 = 8𝑥𝑦  

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/5fdb11b637663f0d6c04f352){:target="_blank"}  

                :octicons-video-16: [Simplifier une expression](https://www.youtube.com/watch?v=eBPOd0bTBro){:target="_blank"}

        ??? question "2) Pourcentage simple"
            50 % d'un nombre, c'est prendre la moitié.  
            25 % d'un nombre, c'est prendre le quart.  
            75 % d'un nombre, c'est prendre les trois quarts.  
            10 % d'un nombre, c'est prendre le dixième.  
            100 % d'un nombre, c'est prendre la totalité.  

            ^^Exemple 1^^ : 10 % de 60 € ?  
            60 ÷ 10 = **6 €**  

            ^^Exemple 2^^ : 25 % de 60 € ?  
            60 ÷ 4 = **15 €**  

            ^^Exemple 3^^ : 75 % de 60 € ?  
            60 ÷ 4 = 15, puis 15 × 3 = **45 €**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC10~o=0,2,4,5~q=0.0,1-2.0,1-4.0,1-5.0,1~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Appliquer un pourcentage](https://www.youtube.com/watch?v=2UVaPRdSMl0){:target="_blank"}

        ??? question "3) Diviser par 10 ; 100 ; 1000"
            Dans la division par 10, 100 ou 1000, le nombre devient plus petit : chaque chiffre perd 1, 2 ou 3 rangs dans l’écriture décimale.  

            ^^Exemple 2^^ : 7 41**3** ÷ 100 = 74,1**3**  
            Le chiffre des unités, le **3**, devient le chiffre des centièmes (division par 100).  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=8NE5~o=0,1,2,3,4,5,6,7,8~q=0.-1.-2.-3.-4.-5.-6.-7.-8.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Diviser par 10 ; 100 ; 1000](https://www.youtube.com/watch?v=pLlBl2V1CC4){:target="_blank"}

        ??? question "4) Priorités de calcul Niveau 3"
            Dans un calcul sans parenthèses et formé uniquement d’additions et de soustractions, les calculs s’effectuent de gauche à droite.  

            ^^Exemple 1^^ : **64 – 9** + 1 = **55** + 1 = 56  
 
            Dans un calcul sans parenthèses, la multiplication et la division sont effectuées en priorité sur l’addition et la soustraction.  
            
            ^^Exemple 2^^ : 5 + **3 × 9** = 5 + **27** = 32  
 
            Dans un calcul avec parenthèses, les calculs entre parenthèses sont effectués en priorité.  
            ^^Exemple 3^^ : (**6 + 7**) × (**14 - 4**) = **13** × **10** = 130  

            !!! quote ""
                :material-weight-lifter: Genially : [10 questions](https://view.genial.ly/5fdb11cfdfd4657a0b8318b9){:target="_blank"}

        ??? question "5) Aire de figure simple"
            L'aire d'une figure est la mesure de sa surface intérieure.  

            - Formule de l'aire d'un carré : côté × côté = côté au carré = c²  
            - Formule de l'aire d'un rectangle : Longueur × largeur = L × l  
            - Formule de l'aire d'un triangle : base × hauteur ÷ 2 = b × h ÷ 2  
            - Formule de l'aire d'un disque : π × rayon × rayon = π × r × r = π × r²  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6MC1~o=0,1,2,3,4~q=0.-1.-2.-3.-4.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Calculer l'aire d'un disque](https://www.youtube.com/watch?v=y-PV5LNmqsM){:target="_blank"}

        ??? question "6) Simplifier une fraction"
            Simplifier une fraction, c’est l’écrire avec le numérateur et le dénominateur le plus petit possible. Quand on ne peut plus simplifier, on dit que l’on a une fraction irréductible.  
            **Méthode** : on regarde si le numérateur et le dénominateur sont dans la même table de multiplication (on décompose ces deux nombres). On peut s'aider des critères de divisibilité.  

            ^^Exemple 1^^ : $\dfrac{14}{6} = \dfrac{7×2}{3×2} = \dfrac{7}{3}$  

            ^^Exemple 2^^ : $\dfrac{35}{21} = \dfrac{7×5}{7×3} = \dfrac{5}{3}$  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=6NB5~o=1~q=1.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Simplifier une fraction](https://www.youtube.com/watch?v=g5oV2wC6RfU){:target="_blank"}

    === ":blue_square: Bleue"
        ??? question "1) Addition et soustraction de fractions Niveau 1"
            On est dans le cas où les dénominateurs sont les mêmes.  
            Pour trouver le résultat, on ajoute les numérateurs et on garde le dénominateur commun.  

            ^^Exemple 1^^ : $\dfrac{5}{3} + \dfrac{8}{3} = \dfrac{5+8}{3} = \dfrac{13}{3}$  

            ^^Exemple 2^^ : $\dfrac{85}{100} - \dfrac{6}{100} = \dfrac{85-6}{100} = \dfrac{79}{100}$  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 15 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC7~o=1~q=1.~p=~t=15~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer une addition ou une soustraction de fractions de même dénominateur](https://www.youtube.com/watch?v=2-JfYiX6Wk4){:target="_blank"}

        ??? question "2) Addition de nombres relatifs"
            Distance à zéro = le nombre sans son signe = partie numérique  
            Si on a deux nombres de **même signe** : on garde le signe commun et on additionne les distances à zéro.  

            ^^Exemple 1^^ : (+3) + (+4) = (+7) ou 7  
            ^^Exemple 2^^ : (−6) + (−7) = (−13)  
 
            Si on a deux nombres de **signes contraires** : on prend le signe du nombre qui a la plus grande distance à zéro et on soustrait les distances à zéro.  

            ^^Exemple 3^^ : (−6) + (+2) = (−4) car 6 est plus grand que 2 donc le résultat est négatif, puis 6 − 2 = 4  
            ^^Exemple 4^^ : (+9) + (−3) = (+6) car 9 est plus grand que 3 donc le résultat est positif, puis 9 − 3 = 6  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 14 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC1~o=1,0~q=0.-1.~p=~t=14~n=6){:target="_blank"}  

                :octicons-video-16: [Somme de deux nombres relatifs](https://www.youtube.com/watch?v=zzhfzRic7xg){:target="_blank"}

        ??? question "3) Carré et cube d'un nombre"
            Le carré d'un nombre est le produit du nombre par lui-même : 𝑎 × 𝑎 = 𝑎².  
            Le cube d'un nombre est le produit du nombre par lui-même et encore par lui même : 𝑎 × 𝑎 × 𝑎 = 𝑎³.  

            ^^Exemple 1^^ : 3² = 3 × 3 = 9   

            ^^Exemple 2^^ : 5² = 5 × 5 = 25  

            ^^Exemple 3^^ : 2³ = 2 × 2 × 2 = 8  

            ^^Exemple 4^^ : 3³ = 3 × 3 × 3 = 27  

        ??? question "4) Je calcule le 3e angle dans un triangle"
            Dans un triangle, la somme de ses 3 mesures d'angle vaut toujours 180°.  

            ^^Exemple^^ : ABC est un triangle tel que $\widehat{A}$ = 52° et $\widehat{B}$ = 25°. Combien mesure $\widehat{C}$ ?  
            Comme 52 + 25 = 77, $\widehat{C}$ mesure 180 - 77 = 103°.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5GB1~o=0,1,2,3~q=0.-1.-2.-3.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Calculer un angle dans un triangle](https://www.youtube.com/watch?v=x0UA6kbiDcM){:target="_blank"}

        ??? question "5) Réduire une expression littérale"
            Réduire une expression littérale, c’est l’écrire avec le moins de termes possibles. On regroupe tous les membres **d’une même famille** ensemble.  

            ^^Exemple 1^^ : 3𝑥 + 4 + 5𝑥 = 4 + 8𝑥 (il y a les termes en 𝑥 ensemble et le nombre seul)  

            ^^Exemple 2^^ : 5𝑥² + 3𝑥 = 5𝑥² + 3𝑥 (car 𝑥 et 𝑥² ne sont pas de la même famille)  

            ^^Exemple 3^^ : 7 × 8𝑥 = 56𝑥 (on peut multiplier les nombres entre-eux, c'est ici de la simplification)  

            ^^Exemple 4^^ : 3𝑥 + 𝑥 = 4𝑥    

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NE6~o=0~q=0.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Réduire une expression littérale](https://www.youtube.com/watch?v=bgpDnvvgBlM){:target="_blank"}

        ??? question "6) Volume de figure simple"
            - Formule du volume d'un cube : côté × côté × côté = c³  
            - Formule du volume d'un pavé droit : Longueur × largeur × hauteur = L × l × h  
            - Formule du volume d'un cylindre : aire(disque) × hauteur = π × rayon × rayon × hauteur = π × r² × h  
            - Formule du volume d'un prisme droit : aire(base) × hauteur  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [pavé droit - 10 questions](https://view.genial.ly/5fdb11e4ebe82c0d17f19111){:target="_blank"}  

                :material-weight-lifter: Maths Mentales : [cylindre et prisme droit - 6 questions](https://view.genial.ly/6077e4d8dc45270d2f8609b2){:target="_blank"}  

                :octicons-video-16: [Calculer le volume d'un cylindre](https://www.youtube.com/watch?v=eJ8BSaTIpYU){:target="_blank"}

    === ":brown_square: Marron"
        ??? question "1) Addition et soustraction de fractions"
            On doit déjà commencer par mettre les fractions au même dénominateur, puis appliquer ce qui a été vu à la ceinture bleue.  
            On va donc multiplier le numérateur ET le dénominateur d’une fraction par un même nombre pour avoir un dénominateur commun.  

            ^^Exemple 1^^ : $\dfrac{4}{6} + \dfrac{5}{3} = \dfrac{4}{6} + \dfrac{5×2}{3×2} = \dfrac{4}{6} + \dfrac{10}{6} = \dfrac{16}{6}$  

            ^^Exemple 2^^ : $\dfrac{6}{15} - \dfrac{1}{5} = \dfrac{6}{15} - \dfrac{1×3}{5×3} = \dfrac{6}{15} - \dfrac{3}{15} = \dfrac{3}{15}$  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC7~o=2~q=2.0,1~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer des additions et soustractions de fractions](https://www.youtube.com/watch?v=wfzLW6oF7VY){:target="_blank"}

        ??? question "2) Soustraction de nombres relatifs"
            Soustraire un nombre relatif, c’est additionner l’opposé de ce nombre relatif.  
            L’opposé de −6 est 6. L’opposé de 3 est −3.  

            ^^Exemple 1^^ : (−6) − (+𝟑) = (−6) + (−𝟑) = −9  
            On garde le premier nombre. Le – de la soustraction devient un + et on change le +3 en son opposé −3.  
 
            ^^Exemple 2^^ : (+5) − (−4) = (+5) + (+4) = +9  
            On garde le premier nombre. Le – de la soustraction devient un + et on change le −4 en son opposé +4.  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NC2~o=0,1~q=0.-1.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Effectuer des additions et soustractions de nombres relatifs](https://www.youtube.com/watch?v=9L4lz1NMPoY){:target="_blank"}

        ??? question "3) Conversion de durées Niveau 2"
            Pour convertir des **durées**, il est important de savoir que :  
            - 60 min = 1 heure  
            - 30 min = ½ heure = 0,5 h  
            - 15 min = ¼ heure = 0,25 h  
            - 45 min = ¾ heure = 0,75 h  

            !!! quote ""
                :material-weight-lifter: Genially : [8 questions](https://view.genial.ly/60769e59e0dc8a0ddbd0e573){:target="_blank"}

        ??? question "4) Conversion aires et volumes"
            Pour convertir des unités d'**aire** : km² - hm² - dam² - m² - dm² - cm² - mm²  
            Le tableau des mesures d'unités d'aire comprend **2** colonnes par unité.  
            1 m² = 1 m × 1 m = 10 dm × 10 dm = 100 dm²  

            ^^Exemple 1^^ : 158,93 m² = 15 893 dm²  
            95,4 cm² = 0,954 dm²  
            167,3 hm² = 1,673 km²  

            Pour convertir des unités de **volume** : km³ - hm³ - dam³ - m³ - dm³ - cm³ - mm³  
            1 dm³ = 1 L
            Le tableau des mesures d'unités de volume comprend **3** colonnes par unité.  
            1 m³ = 1 m × 1 m × 1 m = 10 dm × 10 dm × 10 dm = 1 000 dm³  

            ^^Exemple 2^^ : 50 cm³ = 0,05 dm³  
            8 L = 8 dm³ = 0,008 m³  
            25 m³ = 25 000 dm³ = 25 000 L  
            103,6 m³ = 0,1036 dam³  

            !!! quote ""
                :material-weight-lifter: Genially : [aires - 8 questions](https://view.genial.ly/60769e8c54eed30d321d3d94){:target="_blank"}  

                :material-weight-lifter: Genially : [volumes - 8 questions](https://view.genial.ly/6076a1f748e9c30db23c6ab0){:target="_blank"}  

                :octicons-video-16: [Convertir les unités d'aire](https://www.youtube.com/watch?v=qkDy6lguF80){:target="_blank"}  

                :octicons-video-16: [Convertir les unités de volume](https://www.youtube.com/watch?v=nnXfRWe4WDE){:target="_blank"}  

        ??? question "5) Appliquer un programme de calcul"
            On calcule étape par étape.  

            ^^Exemple^^ :  
            - Choisis un nombre : 5  
            - Ajoute 2 : 7  
            - Multiplie par 3 : 21  

            !!! quote ""
                :material-weight-lifter: Genially : [3 × 7 questions](https://view.genial.ly/6355547a44e22a0019d85c94){:target="_blank"}

        ??? question "6) Calculer en remplaçant x par la valeur indiquée"
            Il s’agit de remplacer 𝑥 par la valeur donnée.  
            Se rappeler que 3𝑥 = 3 × 𝑥 (on refait apparaître la multiplication entre un nombre et une lettre).  

            ^^Exemple 1^^ : Calcule 3𝑥 + 2 pour 𝑥 = 2.  
            3 × 2 + 2 = 6 + 2 = **8**  

            ^^Exemple 2^^ : Calcule 5 + 𝑥 + 4 pour 𝑥 = 4.  
            5 + 4 + 4 = **13**  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 18 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=true_i=5NE3~o=0~q=0.~p=~t=18~n=6){:target="_blank"}  

                :octicons-video-16: [Appliquer une formule (substitution)](https://www.youtube.com/watch?v=FOSVfFdDi7w){:target="_blank"}

    === ":black_large_square: Noire"
        ??? question "1) Décomposition en produit de facteurs premiers"
            Un nombre premier est un nombre qui n’est divisible que par 1 et lui-même.  
            Les plus petits nombres premiers sont 2, 3, 5, 7, 11, 13, 17...  
            Tous les nombres (non premiers) peuvent s’écrire comme une multiplication de nombres premiers.  

            ^^Exemple^^ :  
            22 = 2 × 11  
            6 = 2 × 3  
            9 = 3 × 3  
            16 = 2 × 2 × 2 × 2  
            15 = 3 × 5  

            !!! quote ""
                :material-weight-lifter: Maths Mentales : [6 questions - 20 secondes](https://mathsmentales.net/?i=nothing,e=correction,o=yes,s=1,so=h,f=n,a=,colors=,snd=null&p=0~t=Diapo%201~c=1~o=false_i=5ND1~o=0~q=0.~p=~t=20~n=6){:target="_blank"}  

                :octicons-video-16: [Décomposer un nombre en produits de facteurs premiers](https://www.youtube.com/watch?v=BlGaIqNz_pk){:target="_blank"}   

        ??? question "2) Addition et soustraction de relatifs"
            Voir les ceintures précédentes.  

            !!! quote ""
                :material-weight-lifter: JPPJM : [10 questions](www.jepeuxpasjaimaths.fr/?ex=additionsoustractioncycle4){:target="_blank"}

        ??? question "3) Calcul d’un prix après réduction"
            On calcule d'abord le montant de la réduction, puis on trouve le prix final :  
            Prix final = Prix initial – réduction  

            ^^Exemple^^ : Réduction de 25 % de 40 €.  
            25 % de 40 €, c’est le quart de 40 € donc 40 ÷ 4 = 10 €  
            La réduction est de 10 € donc le prix final est de 40 − 10 = 30 €.  

            !!! quote ""
                :octicons-video-16: [Appliquer un pourcentage](https://www.youtube.com/watch?v=2UVaPRdSMl0){:target="_blank"}  

        ??? question "4) Appliquer des produits en croix"
            ^^Exemple 1^^ :  

            | 4 | 10 |
            |:-:|:-: |
            | 5 | ?  |

            ? = $\dfrac{5×10}{4} = \dfrac{50}{4} = 12,5$  

            ^^Exemple 2^^ :  

            | 6 | ?  |
            |:-:|:-: |
            | 3 | 10 |

            ? = $\dfrac{6×10}{3} = \dfrac{60}{3} = 20$  

        ??? question "5) Test d’égalité"
            Tester une égalité, c’est remplacer la lettre par une valeur qui est donnée et ensuite regarder si l’égalité est vraie ou non.  
            **Méthode** : On calcule **séparément** les 2 membres de l'égalité puis on compare.  
        
            ^^Exemple 1^^ : 3𝑥 = 𝑥 + 2 pour 𝑥 = 2 ?  
            • 3𝑥 = 3 × 2 = 6  
            • 𝑥 + 2 = 2 + 2 = 4  
               6 ≠ 4 donc cette égalité est fausse pour 𝑥 = 2.  
 
            ^^Exemple 2^^ : 4𝑥 = 3𝑥 + 1 pour 𝑥 = 1 ?  
            • 4𝑥 = 4 × 1 = 4  
            • 3𝑥 + 1 = 3 × 1 + 1 = 3 + 1 = 4  
            4 = 4 donc cette égalité est vraie pour 𝑥 = 1.  

            !!! quote ""
                :material-weight-lifter: Genially : [8 questions](https://view.genial.ly/6077e4cadc45270d2f8609ac){:target="_blank"}  

                :octicons-video-16: [Tester une égalité](https://www.youtube.com/watch?v=xZCXVgGT_Bk){:target="_blank"}

        ??? question "6) Nature du triangle"
            - Triangle rectangle : il y a un angle de 90°.  
            - Triangle isocèle : il y a 2 angles de même mesure.  
            - Triangle équilatéral : les 3 angles sont de même mesure : 60°.  



<!-- Objectifs du livret CM -->

:dart: **Les objectifs des ceintures CM :**  
- Meilleure maîtrise des tables de multiplication  
- Meilleure maîtrise des méthodes simples de calcul mental  
- Travailler en temps limité  
- Développer des automatismes  
- Apprendre la logique  
- Apprendre à raisonner  
- Apprendre à travailler en autonomie  

:material-weight-lifter: Les **efforts** que tu fourniras tout au long de l'année pour réussir chacune de ces ceintures t'amèneront à avoir plus de connaissances et plus de facilités à travailler.  

![](../images/logo_cbpm.png){ width=5% }
