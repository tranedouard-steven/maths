---
author: ET
title: 🧊 Le Rubik's cube
hide:
  - footer
---

Voici la méthode débutant pour le résoudre.  

:fontawesome-regular-file-pdf: [Les mouvements avant de commencer](https://nuage03.apps.education.fr/index.php/s/kK4Z2dXTsfsEJdj){:target="_blank"}  

:fontawesome-regular-file-pdf: [Résoudre un Rubik's cube 3 x 3 x 3](https://nuage03.apps.education.fr/index.php/s/DSkykQasNwgS3G6){:target="_blank"}  

:fontawesome-regular-file-pdf: [Résoudre un Rubik's cube 2 x 2 x 2](https://nuage03.apps.education.fr/index.php/s/iM3kSKWBHX2Bn4d){:target="_blank"}  
