---
author: ET
title: 🔱 Orientation
hide:
  - footer
---

!!! abstract "Ressources 3e"
    === "Calendrier"
        L'objectif est de t'aider à préciser ton projet personnel et à te préparer à une filière d’études après la 3e.  

        :construction:  


    === "Lycées de la région"
        :construction:  


??? note "Découverte de métiers"
    === "Avec l'ONISEP"
        - [Consulte en ligne](https://nuage03.apps.education.fr/index.php/s/TgYgLXZQzmrq44c){:target="_blank"} les guides, en complément des brochures papiers disponibles au CDI.  

        - Renseigne-toi sur un métier particulier [sur le site de l'ONISEP](https://www.onisep.fr/recherche?context=metier){:target="_blank"}.  
        
    
    === "Avec Graines d'avenir"
        01. [Découvre les métiers de l'agriculture](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5dc2a36a7d838bb58b5a22d1){:target="_blank"}  

        02. [Découvre les métiers de l'hôtellerie, de la restauration et du tourisme](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5dc532c34a32235ea6bcaae4){:target="_blank"}  

        03. [Découvre les métiers de la santé](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5e68fb6904957e5bb083e41e){:target="_blank"}  

        04. [Découvre le métier d'enseignant](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5fc8e247db14fb44f9894fe5){:target="_blank"}  

        05. [Découvre les métiers du Bâtiment et des Travaux Publics](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5fd228acba99cfccf154f11f){:target="_blank"}  

        06. [Découvre les métiers au service des autres](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5e2829e9daf676b46088b0a7){:target="_blank"}  

        07. [Découvre les métiers de l'informatique et du numérique](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5e550246ecb058f7ae655760){:target="_blank"}  

        08. [Découvre les métiers de la banque et de l'assurance](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5e566d1a26522f2f8c647faf){:target="_blank"}  

        09. [Découvre les métiers de l'information et de la communication](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5e676abee8f4d1281412e7a2){:target="_blank"}  

        10. [Découvre les métiers de la vente et de la relation client](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5e843d52442e0f096497c651){:target="_blank"}  

        11. [Découvre les métiers d'architecte, d'urbaniste et de paysagiste](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5ea99ace154c650e3c2b3749){:target="_blank"}  

        12. [Découvre les métiers de l'industrie](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5ec4d709b91926effc50d59d){:target="_blank"}  

        13. [Découvre les métiers du sport](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5f21201636bdff177a7a1e35){:target="_blank"}  

        14. [Découvre les métiers de l'Économie Sociale et Solidaire](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5f219fcc6958c370d89c3c5f){:target="_blank"}  

        15. [Découvre les métiers de l'environnement et de la transition écologique](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5f21b1de6958c370d89c411a){:target="_blank"}  

        16. [Découvre les métiers du droit et de la justice](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5f6b0903c1690d75f6997cf1){:target="_blank"}  

        17. [Découvre les métiers du transport et de la logistique](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5f6b4d56da3a2c505968fce5){:target="_blank"}  

        18. [Découvre les métiers de l'audiovisuel et du spectacle](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5f7eddf4ed0b0e13c1aa40e1){:target="_blank"}  

        19. [Découvre les métiers d'art](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/5f9001bdcef4af91dfdd4f48){:target="_blank"}  

        20. [Découvre l’entreprenariat](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/601ba2114bbafc51903454ad){:target="_blank"}  

        **Approfondis un secteur : les métiers du droit et de la justice.**  

        21. [Découvre le métier d'avocate et d'avocate](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/6051ce081be9f2afe7e7af28){:target="_blank"}  

        22. [Découvre le métier d'éducatrice et d'éducateur PJJ](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/6051e116cb9b69e10ce03eed){:target="_blank"}  

        23. [Découvre le métier de greffière et de greffier](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/605301d5e57f57b71a476816){:target="_blank"}  

        24. [Découvre le métier de Juriste d'entreprise](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/605312c9e57f57b71a477278){:target="_blank"}  

        25. [Découvre le métier d'officier de la police nationale](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/60a4b00853e2aab794dca518){:target="_blank"}  

        26. [Découvre le métier de lieutenant pénitentiaire](https://grainesdavenir.didask.com/modules/graines-davenir-vf/seeds/60e420856fe07bd7870127a8){:target="_blank"}  


    === "Avec Lien numérique"
        Le site est accessible en [cliquant ici](https://www.concepteursdavenirs.fr/link/lien-numerique/lien-numerique.html){:target="_blank"}.  

        Il propose huit découvertes des métiers du numérique en racontant l'histoire de ce que le numérique rend possible.  

        - Mobilité  
        - Santé  
        - Services  
        - Sécurité  
        - Environnement  
        - Éducation  
        - Aéronautique  
        - Smart cities  