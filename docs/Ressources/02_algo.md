---
author: ET
title: 💻 Compétences numériques
hide:
  - footer
---

??? note "Pix"
    :construction:

??? note "Castor informatique"
    :globe_with_meridians: [Site du Castor informatique pour s'entraîner](https://concours.castor-informatique.fr/){:target="_blank"}

??? note "Algorithmique et programmation"
    === "S'initier à la programmation avec Algoréa"
        |[![](../images/algorea_01.png){ width=50% }](https://concours.castor-informatique.fr/?team=seriousg1){:target="_blank"}|[![](../images/algorea_02.png){ width=50% }](https://concours.castor-informatique.fr/?team=seriousg2){:target="_blank"}|
        |:-:|:-:|
        |[![](../images/algorea_03.png){ width=50% }](https://concours.castor-informatique.fr/?team=seriousg3){:target="_blank"}|[![](../images/algorea_04.png){ width=50% }](https://concours.castor-informatique.fr/?team=seriousg4){:target="_blank"}|
        |[![](../images/algorea_05.png){ width=50% }](https://concours.castor-informatique.fr/?team=seriousg5){:target="_blank"}|[![](../images/algorea_06.png){ width=50% }](https://concours.castor-informatique.fr/?team=seriousg6){:target="_blank"}|
        |[![](../images/algorea_07.png){ width=50% }](https://concours.castor-informatique.fr/?team=seriousg7){:target="_blank"}|[![](../images/algorea_08.png){ width=50% }](https://concours.castor-informatique.fr/?team=seriousg8){:target="_blank"}|  

        Pour reprendre l'une des séances précédentes, saisis ton code en [cliquant ici](https://concours.castor-informatique.fr/?){:target="_blank"}.

    === "D'autres ressources"
        :construction:


??? note "Pour aller plus loin : le langage Python"
    :globe_with_meridians: [Site Pyrates](https://py-rates.fr/){:target="_blank"}
